using System;
using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Localization
{
    public class LocalizationUsingCharacters : LocalizationAlgorithm
    {
        protected override void localize()
        {
            // 1. Progowanie adaptacyjne :
            var adaptativeThresholding = new BradleyLocalThresholding(); /*{ WindowSize = image.Width / 10 };*/
            adaptativeThresholding.ApplyInPlace(image);

            // 2. Odwrocenie kolorow :
            var invert = new Invert();
            invert.ApplyInPlace(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Progowanie");

            // 3. filtracja blobsow ze wzgledu na ksztalt liter:
            var bc = new BlobCounter(image);
            var blobs = bc.GetObjectsInformation();
            var wynik = new List<Blob>();
            foreach (var b in blobs)
            {
                // max sizes
                if (b.Rectangle.Width < image.Width / 20 && b.Rectangle.Width < b.Rectangle.Height &&
                    b.Rectangle.Height > image.Height / 100 && b.Fullness > 0.2f)
                {
                    // max surface :
                    if (b.Rectangle.Width * b.Rectangle.Height < image.Width * image.Width / 100) // 10%x30% WxH
                    {
                        //if (b.Rectangle.Width * b.Rectangle.Height > image.Width / 100 * image.Width / 100)
                        {
                            wynik.Add(b);
                        }
                            /* 
                        else if (3 * b.Rectangle.Width < b.Rectangle.Height)
                        {
                            wynik.Add(b);
                        }*/
                    }

                }
            }
            // 4. Laczenie nie odleglych obszarow :
            connectNearbyBlobs(wynik);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Linie laczace ~litery");

            // 5. Filtrowanie obszar�w :
            var bc2 = new BlobCounter(image);
            var obszary = bc2.GetObjectsInformation();
            wynik = new List<Blob>();

            foreach (var b in obszary)
            {
                //wynik.Add(b);                    
                
                if (b.Rectangle.Width > 1.5 * b.Rectangle.Height && b.Fullness > 0.1f)
                {
                    wynik.Add(b);                    
                }
                /*
                if (b.Rectangle.Width > 2 * b.Rectangle.Height && b.Fullness > 0.2f || true)
                {
                    if (b.Rectangle.Width * b.Rectangle.Height > image.Height * image.Width / 500)
                    {
                        wynik.Add(b);
                    }
                }*/
            }

            // 6. Zaznaczanie tablic
            printPlatesOnImage(wynik);
        }

        private void connectNearbyBlobs(IReadOnlyList<Blob> wynik)
        {
            image = new Bitmap(source.Width, source.Height);
            var g1 = Graphics.FromImage(image);
            g1.Clear(Color.Black);

            for (var y = 0; y < wynik.Count; y++)
            {
                for (var x = 0; x < wynik.Count; x++)
                {
                    if (x != y)
                    {
                        var odl_x = Math.Abs(wynik[x].Rectangle.X - wynik[y].Rectangle.X);
                        var odl_y = wynik[x].Rectangle.Y - wynik[y].Rectangle.Y;
                        
                        if (odl_x < 5 * wynik[y].Rectangle.Width && Math.Abs(odl_y) < 3 * image.Height / 100)
                        {
                            //zgodnosc wysokosci znakow :
                            var stosunek = wynik[y].Rectangle.Height / wynik[x].Rectangle.Height;
                            if (stosunek > 0.75f && stosunek < 1.25f)
                            {
                                /*
                                // rysujemy polaczenie
                                g1.DrawLine(new Pen(Color.White), 
                                    new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y),
                                    new Point(wynik[y].Rectangle.X + wynik[y].Rectangle.Width, wynik[y].Rectangle.Y + wynik[y].Rectangle.Height));
                                    new Point(wynik[y].Rectangle.Right, wynik[y].Rectangle.Y + wynik[y].Rectangle.Height));
                                g1.DrawLine(new Pen(Color.White), 
                                    new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y - 1),
                                    new Point(wynik[y].Rectangle.X + wynik[y].Rectangle.Width, wynik[y].Rectangle.Y - 1 + wynik[y].Rectangle.Height));
                                g1.DrawLine(new Pen(Color.White), 
                                    new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y + 1),
                                    new Point(wynik[y].Rectangle.X + wynik[y].Rectangle.Width, wynik[y].Rectangle.Y - 2 + wynik[y].Rectangle.Height));
                                */
                                g1.DrawLine(new Pen(Color.White),
                                    new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y),
                                    new Point(wynik[y].Rectangle.Right, wynik[y].Rectangle.Bottom));
                                g1.DrawLine(new Pen(Color.White),
                                    new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y - 1),
                                    new Point(wynik[y].Rectangle.Right, wynik[y].Rectangle.Bottom - 1));
                                g1.DrawLine(new Pen(Color.White),
                                    new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y + 1),
                                    new Point(wynik[y].Rectangle.Right, wynik[y].Rectangle.Bottom - 2));

                                // mozna zredukowac przeglad zupelny !! z n^2 do nlogn
                            }
                        }
                    }
                }
            }
        }
    }
}