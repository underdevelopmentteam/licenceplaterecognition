using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AForge.Imaging;
using AForge.Imaging.Filters;

namespace ProjektMagisterski.Localization
{
    public abstract class LocalizationAlgorithm
    {
        #region ProtectedFields
        protected List<Bitmap> probablyPlates;
        protected Bitmap source;
        protected Bitmap image;
        protected int width;
        protected int height;
        #endregion

        #region Getters
        public List<Bitmap> getPlates()
        {
            return probablyPlates;
        }

        public Bitmap getResult()
        {
            return image;
        }
        #endregion

        public bool showOff;

        public LocalizationAlgorithm()
        {
            showOff = false;
            reset();
        }

        public static object[] getAlgorithms()
        {
            return new object[]
            {
                "Op.Morfologiczne",
                "W�asny",
                "Canny",
                "Adaptacyjne",
                "~Density"
            };
        }

        public void reset()
        {
            probablyPlates = new List<Bitmap>();
            source = null;
            image = null;
        }

        public void process(string imagePath)
        {
            try
            {
                source = new Bitmap(imagePath);
                image = new Grayscale(0.2125, 0.7154, 0.0721).Apply(source);

                width = source.Width;
                height = source.Height;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Z�y format pliku!" + ex));
                return;
            }

            localize();
        }

        public void process(Bitmap p_image)
        {
            try
            {
                source = new Bitmap(p_image);
                image = new Grayscale(0.2125, 0.7154, 0.0721).Apply(source);

                width = source.Width;
                height = source.Height;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Z�y format pliku!" + ex));
                return;
            }

            localize();
        }

        protected abstract void localize();

        protected void printPlatesOnImage(IEnumerable<Blob> wynik, bool addMargins = true)
        {
            image = new Bitmap(source);
            var g = Graphics.FromImage(image);
            foreach (var blob in wynik)
            {
                var left = blob.Rectangle.Left;
                var top = blob.Rectangle.Top;
                var rectangleW = blob.Rectangle.Width;
                var rectangleH = blob.Rectangle.Height;
                
                if (addMargins)
                {
                    left = blob.Rectangle.Left - 2 * image.Width / 100;
                    top = blob.Rectangle.Top - 2 * image.Height / 100;
                    rectangleW = blob.Rectangle.Width + 4 * image.Width / 100;
                    rectangleH = blob.Rectangle.Height + 4 * image.Height / 100;
                    if (left < 0) left = 0;
                    if (top < 0) top = 0;   
                }
                
                g.DrawRectangle(new Pen(Color.Red, 3), left, top, rectangleW, rectangleH);
                var wycinanie = new Crop(new Rectangle(left, top, rectangleW, rectangleH));
                probablyPlates.Add(wycinanie.Apply(source));
            }
        }
    }
}