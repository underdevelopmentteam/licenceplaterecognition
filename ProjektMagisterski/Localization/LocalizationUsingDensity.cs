using System;
using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Localization
{
    public class LocalizationUsingDensity : LocalizationAlgorithm
    {
        protected override void localize()
        {
            var inputArray = CommonTools.bitmapToArray(image);
            var Ie = computeIe(inputArray);
            var integralImage = makeIntegral(Ie);
            var densityMap = makeDensity(integralImage, 40, height / 30); // TODO : choose parameters!

            image = CommonTools.arrayToGrayscaleBitmap(densityMap, width, height);

            //if (showOff) FilteringTool.showEfects(new Bitmap(image), "After density");

            if (showOff)
            {
                var test = CommonTools.arrayToBlueRedBitmap(image);
                FilteringTool.showEfects(new Bitmap(test), "before thresholding");
            } 
            
            new Threshold(100).ApplyInPlace(image);

            if (showOff)
            {
                FilteringTool.showEfects(new Bitmap(image), "after thresholding");
            }

            // filtracja
            var bc = new BlobCounter(image);
            var blobs = bc.GetObjectsInformation();
            var wynik = new List<Blob>();

            foreach (var b in blobs)
            {
                if (b.Rectangle.Width > 1.5 * b.Rectangle.Height && b.Rectangle.Width < b.Rectangle.Height * 15)
                {
                    if (b.Area > 16 * image.Height / 100 * image.Width / 100)
                    {
                        wynik.Add(b);
                    }
                }
            }

            printPlatesOnImage(wynik, addMargins: false);
        }

        private int[,] computeIe(int[,] input)
        {
            var result = new int[width, height];

            for (var j = 3; j < height; j++)
            {
                for (var i = 3; i < width; i++)
                {
                    var gradient = 2 * Math.Abs(input[i, j] - input[i - 3, j]) + 0* Math.Abs(input[i, j] - input[i, j - 3]) + Math.Abs(input[i, j] - input[i - 3, j - 3]);
                    //var xxx = Math.Abs(input[i, j] - input[i - 3, j]) - Math.Abs(input[i, j] - input[i, j - 3]);
                    var xxx = 0;
                    result[i, j] = gradient + xxx;
                }
            }

            return result;
        }

        private int[,] makeIntegral(int[,] input)
        {
            var result = new int[width, height];

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var value = input[x, y];

                    if (x - 1 >= 0)
                    {
                        value += result[x - 1, y];
                    }
                    if (y - 1 >= 0)
                    {
                        value += result[x, y - 1];
                    }
                    if (x - 1 >= 0 && y - 1 >= 0)
                    {
                        value -= result[x - 1, y - 1];
                    }

                    result[x, y] = value;
                }
            }

            return result;
        }

        private int[,] makeDensity(int[,] qInput, int windowWidth = 50, int windowHeight = 50)
        {
            var result = new int[width, height];

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var value = 0;

                    var dxLeft = -windowWidth / 2;
                    var dxRight = windowWidth / 2;
                    var dyUp = -windowHeight / 2;
                    var dyDown = windowHeight / 2;

                    if (x - windowWidth / 2 < 0)
                    {
                        dxLeft = -x;
                    }
                    if (x + windowWidth / 2 >= width)
                    {
                        dxRight = width - x - 2;
                    }

                    if (y - windowHeight / 2 < 0)
                    {
                        dyUp = -y;
                    }
                    if (y + windowHeight / 2 >= height)
                    {
                        dyDown = height - y - 2;
                    }


                    value += qInput[x + dxRight, y + dyDown];

                    value += qInput[x + dxLeft, y + dyUp];

                    value -= qInput[x + dxRight, y + dyUp];

                    value -= qInput[x + dxLeft, y + dyDown];

                    result[x, y] = value;
                }
            }

            return result;
        }
    }
}