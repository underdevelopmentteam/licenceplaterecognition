using System;

namespace ProjektMagisterski.Localization
{
    public static class LocalizationFactory
    {
        public static LocalizationAlgorithm create(string type)
        {
            LocalizationAlgorithm result = null;

            switch (findSpecifiedAlgorithm(type))
            {
                case 0: result = new LocalizationUsingMorphology();
                    break;
                case 1: result = new LocalizationUsingGaussian();
                    break;
                case 2: result = new LocalizationUsingCanny();
                    break;
                case 3: result = new LocalizationUsingCharacters();
                    break;
                case 4: result = new LocalizationUsingDensity();
                    break;
            }
            if (result == null)
            {
                throw new Exception("Algorytm nie dodany do fabryki!");
            }
            return result;
        }

        private static int findSpecifiedAlgorithm(string name)
        {
            var data = LocalizationAlgorithm.getAlgorithms();
            for (var i = 0; i < data.Length; i++)
            {
                if ((string) data[i] == name)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}