using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Localization
{
    public class LocalizationUsingMorphology : LocalizationAlgorithm
    {
        protected override void localize()
        {
            image = FilteringTool.getBlackTopHat(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "blackTopHat");

            FilteringTool.horizontalOpening(image);
            FilteringTool.verticalOpening(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "AfterMorphology");

            // progowanie :
            var max_v = FilteringTool.findMaxRedValue(image);
            var threshold = new Threshold(max_v / 2);
            threshold.ApplyInPlace(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Po progowaniu");

            // Filtracja za malych obszarow o zlych proporcjach
            var bc = new BlobCounter(image);
            var blobs = bc.GetObjectsInformation();

            var wynik = new List<Blob>();
            foreach (var b in blobs)
            {
                if (b.Rectangle.Width > 2 * b.Rectangle.Height)
                {
                    if (b.Area > 16 * image.Height / 100 * image.Width / 100)
                    {
                        wynik.Add(b);
                    }
                }
            }

            printPlatesOnImage(wynik);
        }
    }
}