using System;
using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Localization
{
    public class LocalizationUsingCanny : LocalizationAlgorithm
    {
        protected override void localize()
        {
            // 1. Obliczenie H&L thresholds
            var sisThreshold = new SISThreshold();
            var threshold = sisThreshold.CalculateThreshold(image, new Rectangle(0, 0, image.Width, image.Height));

            // 2. Canny:
            var canny = new CannyEdgeDetector
            {
                LowThreshold = Convert.ToByte(8 * threshold / 10),
                HighThreshold = Convert.ToByte(threshold / 2)
            };
            canny.ApplyInPlace(image);

            // 3. Dylatacja
            var dylatacja = new Dilatation();
            dylatacja.ApplyInPlace(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "After canny");

            // 4. Filtracja za malych obszarow
            var BlobFilter = new BlobsFiltering
            {
                MinWidth = 2 * image.Width / 100,
                MinHeight = 1 * image.Width / 100,
                MaxWidth = 50 * image.Width / 100,
                MaxHeight = 40 * image.Height / 100
            };
            BlobFilter.ApplyInPlace(image);
            if (showOff) FilteringTool.showEfects(new Bitmap(image), "After canny + filter");

            // 5. filtracja blobsow :
            var bc = new BlobCounter(image);
            var blobs = bc.GetObjectsInformation();
            var wynik = new List<Blob>();

            foreach (var b in blobs)
            {
                if (b.Rectangle.Width > 2.2f * b.Rectangle.Height && b.Rectangle.Height > image.Height / 100)
                {
                    if (b.Area > 16 * image.Height / 100 * image.Width / 100)
                    {
                        var brightness = b.ColorMean.GetBrightness();
                        var meanRed = b.ColorMean.R;

                        if (brightness > 0.35 && meanRed > 110 &&
                            b.Fullness > 0.1f && b.Fullness < 0.9f)
                        {
                            wynik.Add(b);
                        }
                    }
                }
            }

            printPlatesOnImage(wynik);
        }
    }
}