using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Localization
{
    public class LocalizationUsingGaussian : LocalizationAlgorithm
    {
        protected override void localize()
        {
            // 1.  filtr medianowy, gaussa
            var filterSequence = new FiltersSequence(new Median(), new GaussianBlur());
            image = filterSequence.Apply(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Po filtrze medianowym i gausa 3x3");

            // 3. filtr laplace'a
            int[,] matrix = { { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } };
            var laplasjan = new Convolution(matrix);
            laplasjan.ApplyInPlace(image);
            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Po filtrze Laplace'a");

            // 4. filtr sobela - pionowe krawedzie
            matrix = new[,] { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
            laplasjan = new Convolution(matrix);
            laplasjan.ApplyInPlace(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Przed filtrem gestosciowym");

            // 5. W�asny filtr "gestosciowy"
            var rozmiar = 3 * image.Width / 100;
            if (rozmiar % 2 == 0) rozmiar++;
            laplasjan = new Convolution(FilteringTool.createPiramidKernel(rozmiar));
            laplasjan.ApplyInPlace(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Po filtrze gestosciowym");

            // 6. Binearyzacja metoda SIS - na podstawie histogramu
            var sisThreshold = new SISThreshold();
            sisThreshold.ApplyInPlace(image);

            if (showOff) FilteringTool.showEfects(new Bitmap(image), "Po progowaniu");

            // 7. Filtracja za malych obszarow o zlych proporcjach
            var bc = new BlobCounter(image);
            var blobs = bc.GetObjectsInformation();
            var wynik = new List<Blob>();

            foreach (var b in blobs)
            {
                if (b.Rectangle.Width > 2.5 * b.Rectangle.Height && b.Rectangle.Width < b.Rectangle.Height * 10)
                {
                    if (b.Area > 16 * image.Height / 100 * image.Width / 100)
                    {
                        wynik.Add(b);
                    }
                }
            }

            printPlatesOnImage(wynik);
        }
    }
}