using System;
using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Classification
{
    public class ClassificationUsingDensityPattern : LetterClassifier
    {
        private readonly List<List<int>> wzorceGestosci = new List<List<int>>();

        public ClassificationUsingDensityPattern()
        {
            wczytajGestosci();
        }

        #region PrivateMethods
        private void wczytajGestosci()
        {
            const string dicrectory = "WzorceGestosci\\";
            string path;

            for (var x = '0'; x <= '9'; ++x)
            {
                path = dicrectory + x + ".txt";
                wzorceGestosci.Add(CommonTools.wczytajDaneZPliku(path));
            }

            for (var i = 'a'; i <= 'z'; ++i)
            {
                path = dicrectory + i + ".txt";
                wzorceGestosci.Add(CommonTools.wczytajDaneZPliku(path));
            }
        }

        private int[] gestosci_obrazu(Bitmap p_litera)
        {
            var letter = Grayscale.CommonAlgorithms.BT709.Apply(p_litera);

            var wynik = new int[4, 6]; // square 15x15
            for (int a = 0; a < 4; a++) // dla kazdego kwadratu w poziomie
            {
                for (int b = 0; b < 6; b++) // dla kazdego kwadratu w pionie
                {
                    #region DlaKazdegoPixelaWKwadracie
                    for (int y = b * 15; y < 15 * (b + 1); y++)
                    {
                        for (int x = a * 15; x < 15 * (a + 1); x++)
                        {
                            wynik[a, b] += letter.GetPixel(x, y).R / 100;
                        }
                    }
                    #endregion
                }
            }

            var result = new int[24];

            for (var y = 0; y < 6; y++)
            {
                for (var x = 0; x < 4; x++)
                {
                    result[x + y * 4] = wynik[x, y];
                }
            }

            return result;
        }

        private double[] zaklasyfikuj_litere_gestosci(int[] p_gestosci)
        {
            var wynik = new double[36];

            for (var numer = 0; numer < 36; ++numer)
            {
                wynik[numer] = 0;
                var aktualne = wzorceGestosci[numer];
                for (var i = 0; i < aktualne.Count; ++i)
                {
                    var a = aktualne[i] + 0.01;
                    var b = p_gestosci[i] + 0.01;
                    var relativity = Math.Min(b/a, a/b);
                    
                    if (relativity > 0.3)
                    {
                        wynik[numer] += relativity;
                    }
                }
            }

            return wynik;
        }
        #endregion

        protected override double[] recognize(Bitmap p_letter)
        {
            var result = gestosci_obrazu(p_letter);
            return zaklasyfikuj_litere_gestosci(result);
        }
    }
}