﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;

namespace ProjektMagisterski.Classification
{
    public class RozpoznawanieLiter
    {
        #region PrivateFields
        private ClassificationUsingMarginDistribution rzutyJasnosci;
        private ClassificationUsingDensityPattern wzorceGestosci;
        private ClassificationUsingExhaustiveTemplate exhaustiveTemplate;
        private ClassificationUsingANN neuralNetwork;
        private ClassificationUsingSOM somNetwork;
        #endregion

        public bool show_off;

        public static object[] getAlgorithms()
        {
            return new object[]
            {
                "OCR - rzut jasnosci",
                "OCR - szablony",
                "OCR - SSN",
                "OCR - templates"
                //"OCR - SOM SSN"
            };
        }

        public RozpoznawanieLiter()
        {
            show_off = false;
            const string dir = "SieciNeuronowe\\";
            /*
            SOMLetterClassifier.createNetwork(dir + "ssnSOM", 12*18, 36);
            somNetwork = new SOMLetterClassifier(dir + "ssnSOM");
            somNetwork.uczenieSOM(10000);
            */
            new Thread(() =>
            {
                rzutyJasnosci = new ClassificationUsingMarginDistribution();
                Debug.WriteLine("Rzuty jasnosci loaded");
                wzorceGestosci = new ClassificationUsingDensityPattern();
                Debug.WriteLine("Rzuty gestosci loaded");
                exhaustiveTemplate = new ClassificationUsingExhaustiveTemplate();
                Debug.WriteLine("Templates loaded");
                neuralNetwork = new ClassificationUsingANN(dir + "ssn54x36");
                Debug.WriteLine("Neuralnetwork loaded");
                somNetwork = new ClassificationUsingSOM(dir + "ssnSOM");
                Debug.WriteLine("SOMNetwork loaded");
            }).Start();


            /*
            var nrOfFiles = Directory.GetFiles(dir).Length;

            if (nrOfFiles == 0)
            {
                var filename = dir + "ssn" + nrOfFiles;
                neuralNetwork = new ArtificialNeuralNetwork(Directory.GetFiles(dir)[0]);
                ArtificialNeuralNetwork.createNetwork(filename, 0.5, new int[] { 54, 36 });
            }*/
            //neuralNetwork.uczenie_SSN(1000);
        }

        public IEnumerable<string> rozpoznanie_liter_rzuty(List<List<Bitmap>> listy_liter)
        {
            return rzutyJasnosci.rozpoznaj(listy_liter, show_off);
        }

        public IEnumerable<string> rozpoznanie_liter_gestosci(List<List<Bitmap>> listy_liter)
        {
            return wzorceGestosci.rozpoznaj(listy_liter, show_off);
        }

        public IEnumerable<string> rozpoznanie_liter_wzorce(List<List<Bitmap>> listy_liter)
        {
            return exhaustiveTemplate.rozpoznaj(listy_liter, show_off);
        }

        public IEnumerable<string> rozpoznanie_liter_SSN(List<List<Bitmap>> listy_liter)
        {
            return neuralNetwork.rozpoznaj(listy_liter, show_off);
        }

        public IEnumerable<string> rozpoznanie_liter_SOM_SSN(List<List<Bitmap>> listy_liter)
        {
            return somNetwork.rozpoznaj(listy_liter, show_off);
        }
    }
}