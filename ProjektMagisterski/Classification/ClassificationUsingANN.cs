using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using AForge.Neuro;
using AForge.Neuro.Learning;
using ProjektMagisterski.Tools;
using Image = AForge.Imaging.Image;

namespace ProjektMagisterski.Classification
{
    class ClassificationUsingANN : LetterClassifier
    {
        private readonly string networkName;
        List<double[]> input;
        List<double[]> output;

        private ISupervisedLearning teacher;
        private readonly Network network;

        public static void createNetwork(string filename, double alfa, int[] layersSizes, int inputLength = 216)
        {
            var ssn = new ActivationNetwork(new BipolarSigmoidFunction(alfa), inputLength, layersSizes);
            ssn.Randomize();
            ssn.Save(filename);
        }

        public ClassificationUsingANN(string ssnName)
        {
            networkName = ssnName;
            network = Network.Load(ssnName);
        }

        #region PrivateMethods
        private static double[] convertImageToAnnInput(Bitmap bmp)
        {
            var qImage = new QuickBitmap(bmp);
            qImage.LockBits();

            var wynik1D = new int[12 * 18];
            var index = 0;
            for (var a = 0; a < 12; a++) // dla kazdego kwadratu w poziomie
            {
                for (var b = 0; b < 18; b++) // dla kazdego kwadratu w pionie
                {
                    #region DlaKazdegoPixelaWKwadracie
                    for (var y = b * 5; y < 5 * (b + 1); y++)
                    {
                        for (var x = a * 5; x < 5 * (a + 1); x++)
                        {
                            wynik1D[index] += qImage.GetPixel(x, y).R / 200;
                        }
                    }
                    ++index;
                    #endregion
                }
            }

            qImage.UnlockBits();

            for (var i = 0; i < wynik1D.Length; ++i) // min value 0, max value 25
            {
                wynik1D[i] = 2*wynik1D[i]/25 - 1;
            }

            return Array.ConvertAll(wynik1D, x => (double)x);
        }

        private void generateInputOutputData()
        {
            input = new List<double[]>();
            output = new List<double[]>();

            var dany_znak = new double[37];
            for (var i = 0; i < 37; ++i) dany_znak[i] = -1;

            var indexOfSign = 0;
            for (var sign = '0'; sign <= 'Z'; sign++)
            {
                if (indexOfSign > 0)
                {
                    dany_znak[indexOfSign - 1] = -1;
                }
                dany_znak[indexOfSign] = 1;

                var fileNames = Directory.GetFiles("Litery\\" + sign + "\\");
                foreach (var fileName in fileNames)
                {
                    if (!fileName.EndsWith(".bmp"))
                    {
                        continue;
                    }

                    var temp = Image.FromFile(fileName);
                    input.Add(convertImageToAnnInput(temp));
                    output.Add((double[])dany_znak.Clone());
                }

                if (sign == '9')
                {
                    sign = (char)('A' - 1);
                }
                ++indexOfSign;
            }
        }
        #endregion 

        public void uczenie_SSN(int iterationsLimit = 100000)
        {
            generateInputOutputData();

            var ssn = network as ActivationNetwork;
            teacher = new BackPropagationLearning(ssn);

            var teach = true;
            var iter = 0;

            if (teacher is BackPropagationLearning)
            {
                (teacher as BackPropagationLearning).LearningRate = 1.00f;
                (teacher as BackPropagationLearning).Momentum = 0.1f;    
            }
            else
            {
                (teacher as PerceptronLearning).LearningRate = 1.00f;
            }
            
            while (teach)
            {
                var blad = teacher.RunEpoch(input.ToArray(), output.ToArray());
                Debug.WriteLine("Uczenie sieci - iteracja :" + iter + ", blad :" + blad);

                if (iter % (iterationsLimit / 20)== 0)
                {
                    (teacher as BackPropagationLearning).LearningRate -= 0.05f;
                }
                if (blad < 0.5)
                {
                    teach = false;
                }
                else
                {
                    if (iter < iterationsLimit)
                    {
                        iter++;
                    }
                    else
                    {
                        teach = false;
                    }
                }
            }
            ssn.Save(networkName);
        }

        protected override double[] recognize(Bitmap p_letter)
        {
            var input = convertImageToAnnInput(p_letter);
            return network.Compute(input);
        }
    }
}