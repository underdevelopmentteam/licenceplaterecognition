using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Classification
{
    public class ClassificationUsingMarginDistribution : LetterClassifier
    {
        private readonly List<List<int>> wzorceRzutowJasnosci = new List<List<int>>();

        public ClassificationUsingMarginDistribution(string samplesDirectory = "WzorceJasnosci\\")
        {
            WczytajRzutyJasnosci(samplesDirectory);
        }

        #region Private
        private void WczytajRzutyJasnosci(string directory = "WzorceJasnosci\\")
        {
            for (var i = '0'; i <= '9'; ++i)
            {
                var pathToFile = directory + "R" + i + ".txt";
                wzorceRzutowJasnosci.Add(CommonTools.wczytajDaneZPliku(pathToFile));
            }

            for (var i = 'A'; i <= 'Z'; ++i)
            {
                var pathToFile = directory + "R" + i + ".txt";
                wzorceRzutowJasnosci.Add(CommonTools.wczytajDaneZPliku(pathToFile));
            }

        }

        private static int[] rzut_jasnosci_obrazu(Bitmap litera)
        {
            var grayImage = Grayscale.CommonAlgorithms.BT709.Apply(litera);

            var Vstats = new VerticalIntensityStatistics(grayImage);
            var Hstats = new HorizontalIntensityStatistics(grayImage);

            var Vhistogram = Vstats.Gray;
            var Hhistogram = Hstats.Gray;
            var verticalInt = Vhistogram.Values;
            var horizontalInt = Hhistogram.Values;

            var result = new int[verticalInt.Length + horizontalInt.Length];

            for (var i = 0; i < verticalInt.Length; ++i)
            {
                result[i] = verticalInt[i];
            }

            var j = 0;
            for (var i = verticalInt.Length; i < verticalInt.Length + horizontalInt.Length; ++i)
            {
                result[i] = horizontalInt[j++];
            }

            return result;
        }

        private int maxValue(int[] wzorzecLitery, int lastIndex)
        {
            var maxV = wzorzecLitery[lastIndex];

            for (int i = lastIndex - 4; i <= lastIndex; i++)
            {
                if (wzorzecLitery[lastIndex] > maxV)
                {
                    maxV = wzorzecLitery[lastIndex];
                }
            }
            return maxV;
        }

        private int minValue(int[] wzorzecLitery, int lastIndex)
        {
            var minV = wzorzecLitery[lastIndex];

            for (int i = lastIndex - 4; i <= lastIndex; i++)
            {
                if (wzorzecLitery[lastIndex] < minV)
                {
                    minV = wzorzecLitery[lastIndex];
                }
            }
            return minV;
        }

        private double[] zaklasyfikuj_litere_rzuty(int[] rzutJasnosci)
        {
            var wynik = new double[36];

            for (var numer = 0; numer < 36; ++numer)
            {
                wynik[numer] = 0;
                var wzorzecLitery = wzorceRzutowJasnosci[numer];

                for (var i = 4; i < wzorzecLitery.Count; ++i)
                {
                    var WzorzecminV = minValue(wzorzecLitery.ToArray(), i) + 0.01;
                    var WzorzecmaxV = maxValue(wzorzecLitery.ToArray(), i) + 0.01;

                    var RzutMinV = minValue(rzutJasnosci, i) + 0.01;
                    var RzutMaxV = maxValue(rzutJasnosci, i) + 0.01;

                    var liczbaMin = Math.Min(WzorzecminV / RzutMinV, RzutMinV / WzorzecminV);
                    var liczbaMax = Math.Min(WzorzecmaxV / RzutMaxV, RzutMaxV / WzorzecmaxV);

                    wynik[numer] += (liczbaMin + liczbaMax) / 2;
                }
            }

            return wynik;
        }
        #endregion

        protected override double[] recognize(Bitmap p_letter)
        {
            var rzutJasnosci = rzut_jasnosci_obrazu(p_letter);
            return zaklasyfikuj_litere_rzuty(rzutJasnosci);
        }
    }
}