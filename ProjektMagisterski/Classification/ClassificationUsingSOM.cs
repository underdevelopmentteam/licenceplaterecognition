using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using AForge.Neuro;
using AForge.Neuro.Learning;
using ProjektMagisterski.Tools;
using Image = AForge.Imaging.Image;

namespace ProjektMagisterski.Classification
{
    public class ClassificationUsingSOM : LetterClassifier
    {
        public double learningError;

        private List<double[]> input;
        private readonly DistanceNetwork network;
        private readonly string name;

        public ClassificationUsingSOM(string networkName)
        {
            name = networkName;
            network = Network.Load(networkName) as DistanceNetwork;
        }

        public static void createNetwork(string fileName, int inputCount, int nrOfNeurons)
        {
            var ssn = new DistanceNetwork(inputCount, nrOfNeurons);
            ssn.Randomize();
            ssn.Save(fileName);
        }

        protected override double[] recognize(Bitmap p_letter)
        {
            var annInput = convertImageToAnnInput(p_letter);
            return network.Compute(annInput);
        }

        public void uczenieSOM(int iterations = 1000)
        {
            generateInputData();
            var trainer = new SOMLearning(network, 6, 6);

            const double learningRate = 0.2;
            const double fixedLearningRate = learningRate / 10;
            const double driftingLearningRate = fixedLearningRate * 9;
            const double learningRadius = 0.5;

            var errors = new List<double>();

            // iterations
            var i = 0;

            //bool seeRandom = true;
            //learningError = double.MaxValue;

            while ( i < iterations)
            {

                //if (seeRandom)
                //{
                //   trainer.LearningRate = new Random().NextDouble();
                //    trainer.LearningRadius = new Random().NextDouble()*7;
                //}
                //else
                //{
                    //trainer.LearningRate *= 0.9999;
                    //trainer.LearningRadius *= 0.9999;
                trainer.LearningRate = driftingLearningRate * (iterations - i) / iterations + fixedLearningRate;
                trainer.LearningRadius = learningRadius * (iterations - i) / iterations;    
                //}
                
                // run training epoch
                learningError = trainer.RunEpoch(input.ToArray());
                errors.Add(learningError);
                
                //if (learningError < 50000)
                //{
                //    seeRandom = false;
                //}
                // increase current iteration
                i++;
            }

            network.Save(name);
        }
        
        #region PrivateMethods
        private static double[] convertImageToAnnInput(Bitmap bmp)
        {
            var qImage = new QuickBitmap(bmp);
            qImage.LockBits();

            var wynik1D = new int[12 * 18];
            var index = 0;
            for (var a = 0; a < 12; a++) // dla kazdego kwadratu w poziomie
            {
                for (var b = 0; b < 18; b++) // dla kazdego kwadratu w pionie
                {
                    #region DlaKazdegoPixelaWKwadracie
                    for (var y = b * 5; y < 5 * (b + 1); y++)
                    {
                        for (var x = a * 5; x < 5 * (a + 1); x++)
                        {
                            wynik1D[index] += qImage.GetPixel(x, y).R / 200;
                        }
                    }
                    ++index;
                    #endregion
                }
            }

            qImage.UnlockBits();

            for (var i = 0; i < wynik1D.Length; ++i) // min value 0, max value 25
            {
                wynik1D[i] = 2 * wynik1D[i] / 25 - 1;
            }

            return Array.ConvertAll(wynik1D, x => (double)x);
        }

        private void generateInputData()
        {
            input = new List<double[]>();
            
            var dany_znak = new double[37];
            for (var i = 0; i < 37; ++i) dany_znak[i] = -1;

            var indexOfSign = 0;
            for (var sign = '0'; sign <= 'Z'; sign++)
            {
                if (indexOfSign > 0)
                {
                    dany_znak[indexOfSign - 1] = -1;
                }
                dany_znak[indexOfSign] = 1;

                var fileNames = Directory.GetFiles("Litery\\" + sign + "\\");
                foreach (var fileName in fileNames)
                {
                    if (!fileName.EndsWith(".bmp"))
                    {
                        continue;
                    }

                    var temp = Image.FromFile(fileName);
                    input.Add(convertImageToAnnInput(temp));
                }

                if (sign == '9')
                {
                    sign = (char)('A' - 1);
                }
                ++indexOfSign;
            }
        }
        #endregion
    }
}