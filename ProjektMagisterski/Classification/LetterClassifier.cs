using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Classification
{
    public abstract class LetterClassifier
    {
        protected abstract double[] recognize(Bitmap p_letter);

        protected char classifyResult(double[] wynik)
        {
            // brak Q
            wynik[26] = -1;
            var maxValue = double.MinValue;
            var index = 0;
            for (var i = 0; i < 36; ++i)
            {
                if (wynik[i] > maxValue)
                {
                    maxValue = wynik[i];
                    index = i;
                }
            }
            // tutaj po "index" ustalamy wskazana litere.
            char[] tabofresult = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            return tabofresult[index];
        }

        public IEnumerable<string> rozpoznaj(List<List<Bitmap>> listy_liter, bool showSteps = true)
        {
            var licensePlateNumbers = new List<string>();

            if (!showSteps) // multithread
            {
                foreach (var listaLiter in listy_liter)
                {
                    if (listaLiter==null)
                    {
                        licensePlateNumbers.Add("skip");
                        continue;
                    }
                    var napis = new List<char>();
                    for (int j = 0; j < listaLiter.Count; j++)
                    {
                        napis.Add(Convert.ToChar(" "));
                    }

                    Parallel.For(0, listaLiter.Count, iter =>
                    {
                        napis[iter] = Convert.ToChar(" ");
                        var result = recognize(FilteringTool.convertToLetterStandard(listaLiter[iter]));
                        napis[iter] = classifyResult(result);
                    });

                    var str = "";

                    foreach (var c in napis)
                    {
                        str += c.ToString();
                    }

                    licensePlateNumbers.Add(str);
                }
            }
            else // single thread
            {
                foreach (var bmps in listy_liter)
                {
                    if (bmps == null){ continue;}
                    var napis = "";
                    foreach (var bmp in bmps)
                    {
                        var result = recognize(FilteringTool.convertToLetterStandard(bmp));
                        napis += classifyResult(result);

                        var chart = CommonTools.generateChart(result, new Bitmap(bmp));
                        FilteringTool.showEfects(chart, "wynik : " + napis[napis.Length - 1]);

                    }
                    licensePlateNumbers.Add(napis);
                }
            }

            return licensePlateNumbers;
        }
    }
}