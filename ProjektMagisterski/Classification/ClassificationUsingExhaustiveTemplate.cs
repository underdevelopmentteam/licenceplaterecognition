using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using AForge.Imaging;

namespace ProjektMagisterski.Classification
{
    public class ClassificationUsingExhaustiveTemplate : LetterClassifier
    {
        readonly List<Bitmap> wzorce = new List<Bitmap>();

        public ClassificationUsingExhaustiveTemplate()
        {
            wczytajWzorce();
        }

        private void wczytajWzorce()
        {
            const string path = "WzorcoweLitery\\";

            for (var i = '0'; i <= '9'; i++)
            {
                var sciezka = path + i + ".bmp";
                wzorce.Add(new Bitmap(sciezka));
            }
            for (var i = 'A'; i <= 'Z'; i++)
            {
                var sciezka = path + i + ".bmp";
                wzorce.Add(new Bitmap(sciezka));
            }
        }

        private readonly object locker = new object();

        protected override double[] recognize(Bitmap p_letter)
        {
            var template = new ExhaustiveTemplateMatching { SimilarityThreshold = 0.3f };

            p_letter = p_letter.Clone(new Rectangle(0, 0, p_letter.Size.Width, p_letter.Size.Height), PixelFormat.Format8bppIndexed);

            var wynik = new double[wzorce.Count];

            for (var x = 0; x < wzorce.Count; x++)
            {
                Bitmap wzorzec;
                lock (locker)
                {
                    wzorzec = wzorce[x].Clone(new Rectangle(0, 0, p_letter.Size.Width, p_letter.Size.Height), PixelFormat.Format8bppIndexed);
                }

                var result = template.ProcessImage(p_letter, wzorzec);

                if (result.Length > 0)
                {
                    wynik[x] = (template.ProcessImage(p_letter, wzorzec))[0].Similarity;
                }
                else
                {
                    wynik[x] = 0.0f;
                }

            }

            return wynik;
        }
    }
}