﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ProjektMagisterski.Classification;
using ProjektMagisterski.Localization;
using ProjektMagisterski.Segmentation;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski
{
// ReSharper disable once InconsistentNaming
    public class ALPRecognition
    {
        private LocalizationAlgorithm localizationAlgorithm;
        private SegmentationAlgorithm segmentationAlgorithm;
        private readonly RozpoznawanieLiter recognizeLetters;

        public ALPRecognition()
        {
            recognizeLetters = new RozpoznawanieLiter();
        }

        public void selectLocalizationAlgorithm(string name)
        {
            localizationAlgorithm = LocalizationFactory.create(name);
        }

        public void selectSegmentationAlgorithm(string name)
        {
            segmentationAlgorithm = SegmentationFactory.create(name);
        }

        public void localizePlate(string filePath, bool showOff = false)
        {
            FilteringTool.showEfects(null, "reset");

            localizationAlgorithm.reset();
            localizationAlgorithm.showOff = showOff;
            localizationAlgorithm.process(filePath);
        }

        public void localizePlate(Bitmap p_image, bool showOff = false)
        {
            FilteringTool.showEfects(null, "reset");

            localizationAlgorithm.reset();
            localizationAlgorithm.showOff = showOff;
            localizationAlgorithm.process(p_image);
        }

        public void segmentatePlates(bool showOff = false)
        {
            FilteringTool.showEfects(null, "reset");

            segmentationAlgorithm.showOff = showOff;
            var plates = localizationAlgorithm.getPlates();
            if (plates.Count == 0)
            {
                return;
            }
            segmentationAlgorithm.process(plates);
        }

        public List<Bitmap> getPlateCandidates()
        {
            return localizationAlgorithm.getPlates();
        }

        public List<string> classifyLetters(int selectedItem, bool showOff = false)
        {
            FilteringTool.showEfects(null, "reset");

            recognizeLetters.show_off = showOff;

            var letters = segmentationAlgorithm.getPlateLetters();

            switch (selectedItem)
            {
                case 0:
                    return recognizeLetters.rozpoznanie_liter_rzuty(letters) as List<string>;
                case 1:
                    return recognizeLetters.rozpoznanie_liter_gestosci(letters) as List<string>;
                case 2:
                    return recognizeLetters.rozpoznanie_liter_SSN(letters) as List<string>;
                case 3:
                    return recognizeLetters.rozpoznanie_liter_wzorce(letters) as List<string>;
                case 4:
                    return recognizeLetters.rozpoznanie_liter_SOM_SSN(letters) as List<string>;
            }
            throw new Exception("brak algorytmu");
        }

        public Bitmap getLocalizedPlate()
        {
            if (localizationAlgorithm != null)
            {
                return localizationAlgorithm.getResult();                
            }

            return null;
        }
        
        public List<List<Bitmap>> getPlateLetters()
        {
            return segmentationAlgorithm.getPlateLetters();
        }

        public void resetSegmentation()
        {
            segmentationAlgorithm.reset();
        }
    }
    
}
