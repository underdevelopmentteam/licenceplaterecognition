using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;

namespace ProjektMagisterski.Tools
{
    public static class CommonTools
    {
        public static List<int> wczytajDaneZPliku(string nazwa)
        {
            var temp = new List<int>();
            var linie = File.ReadAllLines(nazwa);
            foreach (string linia in linie)
            {
                foreach (string napis in linia.Split(' '))
                {
                    if (napis != "")
                    {
                        temp.Add(int.Parse(napis));
                    }
                }
            }
            return new List<int>(temp);
        }

        public static Bitmap generateChart(double[] results, Bitmap input)
        {
            const int width = 930;
            const int height = 500;
            var result = new Bitmap(width, height);

            var g = Graphics.FromImage(result);
            g.DrawImage(input, new Point(0, 0));

            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            
            var text = "";
            for (var c = '0'; c <= 'Z'; c++)
            {
                text += c;
                if (c == '9')
                {
                    c = (char)('A' - 1);
                }
            }

            var pair = findMinAndMax(results);
            var yPosition = input.Height + 50;
            const int dist = 25;

            for (var i = 0; i < results.Length; i++)
            {
                var v = 300*Math.Abs(results[i] - pair.X)/(pair.Y - pair.X);
                var barColor = Math.Abs(results[i] - pair.Y) < 0.01 ? Color.Red : Color.Blue;

                var brush = new SolidBrush(barColor);

                g.DrawString(text[i].ToString(CultureInfo.InvariantCulture), new Font("Tahoma", 20), Brushes.Black, new RectangleF(i*dist, yPosition - 30, 30, 30));
                g.FillRectangle(brush, new Rectangle(i*dist + 2, yPosition, 20, (int)v));
            }

            g.DrawLine(new Pen(Color.Red), new PointF(0, yPosition + 300), new PointF(width - 1, yPosition + 300));
            g.DrawString("100%", new Font("Tahoma", 20), Brushes.Red, new RectangleF(0, yPosition + 310, width, 40));

            g.Flush();
            

            return result;
        }

        private static PointF findMinAndMax(double[] results)
        {
            var minV = double.MaxValue;
            var maxV = double.MinValue;
            foreach (var result in results)
            {
                if (result > maxV)
                {
                    maxV = result;
                }
                if (result < minV)
                {
                    minV = result;
                }
            }
            return new PointF((float)minV, (float)maxV);
        }

        public static Bitmap arrayToBlueRedBitmap(Bitmap array)
        {
            var width = array.Width; var height = array.Height;

            var result = new Bitmap(width, height);
            var qResult = new QuickBitmap(result);
            qResult.LockBits();

            var qArray = new QuickBitmap(array);
            qArray.LockBits();

            var maxValue = int.MinValue;
            var minValue = int.MaxValue;

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    if (qArray.GetPixel(x, y).R > maxValue)
                    {
                        maxValue = qArray.GetPixel(x, y).R;
                    }
                    if (qArray.GetPixel(x, y).R < minValue)
                    {
                        minValue = qArray.GetPixel(x, y).R;
                    }
                }
            }

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var v = (double)(qArray.GetPixel(x, y).R - minValue) / (maxValue - minValue);
                    qResult.SetPixel(x, y, convertToBlueRedScale(v));
                }
            }

            qArray.UnlockBits();
            qResult.UnlockBits();
            return result;
        }

        public static Bitmap arrayToGrayscaleBitmap(int[,] array, int width, int height)
        {
            var result = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
            var qResult = new QuickBitmap(result);
            qResult.LockBits();

            var maxValue = int.MinValue;
            var minValue = int.MaxValue;

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    if (array[x, y] > maxValue)
                    {
                        maxValue = array[x, y];
                    }
                    if (array[x, y] < minValue)
                    {
                        minValue = array[x, y];
                    }
                }
            }

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var v = 255 * (double)(array[x, y] - minValue) / (maxValue - minValue);
                    if (maxValue - minValue == 0)
                    {
                        qResult.SetPixel(x, y, Color.FromArgb(0, 0, 0));
                    }
                    else
                    {
                        qResult.SetPixel(x, y, Color.FromArgb((int)v, (int)v, (int)v));                        
                    }
                }
            }

            qResult.UnlockBits();
            return result;
        }

        private static Color convertToBlueRedScale(double k)
        {
            if (k < 0) k = 0;
            if (k > 1) k = 1;

            double r, g, b;
            if (k < 0.25)
            {
                r = 0;
                g = 4 * k;
                b = 1;
            }
            else if (k < 0.5)
            {
                r = 0;
                g = 1;
                b = 1 - 4 * (k - 0.25);
            }
            else if (k < 0.75)
            {
                r = 4 * (k - 0.5);
                g = 1;
                b = 0;
            }
            else
            {
                r = 1;
                g = 1 - 4 * (k - 0.75);
                b = 0;
            }

            var R = (byte)(r * 255 + 0.0);
            var G = (byte)(g * 255 + 0.0);
            var B = (byte)(b * 255 + 0.0);

            return Color.FromArgb(R, G, B);
        }

        public static int[,] bitmapToArray(Bitmap input)
        {
            var result = new int[input.Width, input.Height];
            var qinput = new QuickBitmap(input);
            qinput.LockBits();
            for (int y = 0; y < input.Height; y++)
            {
                for (int x = 0; x < input.Width; x++)
                {
                    result[x, y] = qinput.GetPixel(x, y).R;
                }
            }
            qinput.UnlockBits();
            return result;
        }
    }
}