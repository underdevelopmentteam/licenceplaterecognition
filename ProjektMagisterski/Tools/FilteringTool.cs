﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using AForge.Imaging.Filters;

namespace ProjektMagisterski.Tools
{
    public static class FilteringTool
    {
        private static ImageViewer resultViewer;

        public static int[,] createPiramidKernel(int p_rozmiar)
        {
            var kernel = new int[p_rozmiar, p_rozmiar];
            var start = 0;
            for (var s = 0; s < p_rozmiar / 2; ++s)
            {
                for (var y = start; y < p_rozmiar - start; ++y)
                {
                    for (var x = start; x < p_rozmiar - start; ++x)
                    {
                        kernel[y, x] = start * 2 + 1;
                    }
                }
                start++;
            }
            return kernel;
        }

        public static void showEfects(Bitmap p_image, String p_text)
        {
            if (p_text == "reset")
            {
                resultViewer = null;
                return;
            }

            if (resultViewer == null)
            {
                resultViewer = new ImageViewer(p_image, p_text);
                resultViewer.Show();
            }
            else
            {
                resultViewer.addImage(p_image, p_text);
            }
        }

        public static void verticalOpening(Bitmap p_image)
        {
            // 1%
            var dylX = new short[,] { { -1, 1, -1 }, { -1, 1, -1 }, { -1, 1, -1 } };
            var dylatacja = new Dilatation(dylX);
            var erozja = new Erosion(dylX);

            for (var x = 0; x < p_image.Height * 1 / 100; ++x)
            {
                erozja.ApplyInPlace(p_image);
            }
            for (var x = 0; x < p_image.Height * 1 / 100; ++x)
            {
                dylatacja.ApplyInPlace(p_image);
            }
        }

        public static void horizontalOpening(Bitmap p_image)
        {
            // 3%
            var dylX = new short[,] { { -1, -1, -1 }, { 1, 1, 1 }, { -1, -1, -1 } };
            var dylatacja = new Dilatation(dylX);
            for (var x = 0; x < p_image.Width * 3 / 100; ++x)
            {
                dylatacja.ApplyInPlace(p_image);
            }
            var erozja = new Erosion(dylX);
            for (var x = 0; x < p_image.Width * 3 / 100; ++x)
            {
                erozja.ApplyInPlace(p_image);
            }
        }

        public static int findMaxRedValue(Bitmap p_image)
        {
            var maxV = 0;
            for (var y = 0; y < p_image.Height; ++y)
            {
                for (var x = 0; x < p_image.Width; ++x)
                {
                    if (p_image.GetPixel(x, y).R > maxV)
                    {
                        maxV = p_image.GetPixel(x, y).R;
                    }
                }
            }
            return maxV;
        }

        public static Bitmap getBlackTopHat(Bitmap p_image)
        {
            var dylatacja = new Dilatation();
            var erozja = new Erosion();

            var temp = new Bitmap(p_image);
            temp = Grayscale.CommonAlgorithms.BT709.Apply(temp);
            var temp2 = new Bitmap(temp);
            temp2 = Grayscale.CommonAlgorithms.BT709.Apply(temp2);
            dylatacja.ApplyInPlace(temp);
            dylatacja.ApplyInPlace(temp);
            dylatacja.ApplyInPlace(temp);
            erozja.ApplyInPlace(temp);
            erozja.ApplyInPlace(temp);
            erozja.ApplyInPlace(temp);
            var odejmowanie = new Subtract(temp2);
            odejmowanie.ApplyInPlace(temp);
            return temp;
        }

        public static Bitmap convertToLetterStandard(Bitmap p_letter)
        {
            var fs = new FiltersSequence
            {
                new GrayscaleBT709(),
                new SISThreshold(),
                new ResizeBicubic(60, 90)
            };

            try
            {
                p_letter = fs.Apply(p_letter);
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format("Cannot convert image"));
            }

            return new Bitmap(p_letter);
        }
    }
}