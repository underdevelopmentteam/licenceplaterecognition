using System;
using System.Drawing;

namespace ProjektMagisterski.Tools
{
    public class QuickBitmap
    {
        #region Private Fields
        private Bitmap source = null;
        private IntPtr Iptr = IntPtr.Zero;
        private System.Drawing.Imaging.BitmapData bitmapData = null;
        private byte[] Pixels { get; set; }
        private int Depth { get; set; }
        private int Width { get; set; }
        private int Height { get; set; }
        #endregion

        #region Constructor
        public QuickBitmap(Bitmap image)
        {
            source = image;
        }
        #endregion

        public void LockBits()
        {
            try
            {
                Width = source.Width;
                Height = source.Height;

                int ilosc_px = Width * Height;
                var figura = new Rectangle(0, 0, Width, Height);
                Depth = Bitmap.GetPixelFormatSize(source.PixelFormat);

                if (Depth != 8 && Depth != 24 && Depth != 32)
                {
                    throw new ArgumentException("Z�y typ obrazu!");
                }
                bitmapData = source.LockBits(figura, System.Drawing.Imaging.ImageLockMode.ReadWrite, source.PixelFormat);
                int step = Depth / 8;
                Pixels = new byte[ilosc_px * step];
                Iptr = bitmapData.Scan0;
                System.Runtime.InteropServices.Marshal.Copy(Iptr, Pixels, 0, Pixels.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void UnlockBits()
        {
            try
            {
                System.Runtime.InteropServices.Marshal.Copy(Pixels, 0, Iptr, Pixels.Length);
                source.UnlockBits(bitmapData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Color GetPixel(int x, int y)
        {
            var clr = new Color();
            int cCount = Depth / 8;

            int i = ((y * Width) + x) * cCount;

            if (i > Pixels.Length - cCount)
            {
                throw new IndexOutOfRangeException();
            }

            byte a = 0, r = 0, g = 0, b = 0;
            switch (Depth)
            {
                case 32:
                    b = Pixels[i];
                    g = Pixels[i + 1];
                    r = Pixels[i + 2];
                    a = Pixels[i + 3];
                    clr = Color.FromArgb(a, r, g, b);
                    break;
                case 24:
                    b = Pixels[i];
                    g = Pixels[i + 1];
                    r = Pixels[i + 2];
                    clr = Color.FromArgb(r, g, b);
                    break;
                case 8:
                    b = Pixels[i];
                    clr = Color.FromArgb(b, b, b);
                    break;
            }
            return clr;
        }

        public void SetPixel(int x, int y, Color c)
        {
            int cCount = Depth / 8;

            int i = ((y * Width) + x) * cCount;

            if (i > Pixels.Length - cCount)
            {
                throw new IndexOutOfRangeException();
            }

            byte a = c.A, r = c.R, g = c.G, b = c.B;

            switch (Depth)
            {
                case 32:
                    Pixels[i] = b;
                    Pixels[i + 1] = g;
                    Pixels[i + 2] = r;
                    Pixels[i + 3] = a;
                    break;
                case 24:
                    Pixels[i] = b;
                    Pixels[i + 1] = g;
                    Pixels[i + 2] = r;
                    break;
                case 8:
                    Pixels[i] = b;
                    break;
            }
        }
    }
}