﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ProjektMagisterski.Classification;
using ProjektMagisterski.Localization;
using ProjektMagisterski.Segmentation;

namespace ProjektMagisterski
{
    public partial class MainWindow : Form
    {
        readonly ALPRecognition recognition = new ALPRecognition();

        private string fileName;

        readonly Stopwatch zegarek = new Stopwatch();

        public MainWindow()
        {
            InitializeComponent();

            sourcePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            sourcePictureBox.Update();
            sourcePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            sourcePictureBox.Update();


            comboBox1.Items.AddRange(LocalizationAlgorithm.getAlgorithms());
            comboBox1.SelectedIndex = 0;

            comboBox2.Items.AddRange(SegmentationAlgorithm.getAlgorithms());
            comboBox2.SelectedIndex = 0;

            comboBox3.Items.AddRange(RozpoznawanieLiter.getAlgorithms());
            comboBox3.SelectedIndex = 0;

            checkBoxShowSteps.Enabled = false;
        }

        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            var Okno_wczytywania_pliku = new OpenFileDialog();
            if (Okno_wczytywania_pliku.ShowDialog() == DialogResult.OK)
            {
                fileName = Okno_wczytywania_pliku.FileName;
                groupBoxAlgorithms.Enabled = true;
                checkBoxShowSteps.Enabled = true;
                sourcePictureBox.Image = new Bitmap(fileName);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            fileName = "";
            groupBoxAlgorithms.Enabled = false;
            checkBoxShowSteps.Enabled = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selection = comboBox1.SelectedItem as string;
            recognition.selectLocalizationAlgorithm(selection);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selection = comboBox2.SelectedItem as string;
            recognition.selectSegmentationAlgorithm(selection);
        }

        private void btnLocalize_Click(object sender, EventArgs e)
        {
            zegarek.Restart();
            recognition.localizePlate(fileName, checkBoxShowSteps.Checked);
            zegarek.Stop();

            textBox1.Text = string.Format("{0}ms", zegarek.ElapsedMilliseconds);
            sourcePictureBox.Image = recognition.getLocalizedPlate();
            sourcePictureBox.Update();
        }

        private void btnExtract_Click(object sender, EventArgs e)
        {
            zegarek.Restart();
            recognition.segmentatePlates(checkBoxShowSteps.Checked);
            zegarek.Stop();

            textBox2.Text = string.Format("{0}ms", zegarek.ElapsedMilliseconds);

            var nrOfLetters = recognition.getPlateLetters().Sum(plate => plate!=null?plate.Count:0);

            textBox4.Text = string.Format("Z={0}, T={1}", nrOfLetters, recognition.getPlateLetters().Count);
        }

        private void btnRecognize_Click(object sender, EventArgs e)
        {
            var napisy = new List<string>();
            listBoxResults.Items.Clear();

            zegarek.Restart();
            napisy = recognition.classifyLetters(comboBox3.SelectedIndex, checkBoxShowSteps.Checked);
            zegarek.Stop();

            textBox3.Text = string.Format("{0}ms", zegarek.ElapsedMilliseconds);

            foreach (var txt in napisy)
            {
                listBoxResults.Items.Add(txt);
            }
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBoxSettings_Enter(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBoxResults_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBoxMeasurements_Enter(object sender, EventArgs e)
        {

        }
    }
}
