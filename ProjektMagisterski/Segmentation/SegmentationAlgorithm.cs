using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Segmentation
{
    public abstract class SegmentationAlgorithm
    {
        public bool showOff;

        protected List<List<Bitmap>> plateLetters;

        public List<List<Bitmap>> getPlateLetters()
        {
            return plateLetters;
        }

        public void reset()
        {
            plateLetters = new List<List<Bitmap>>();
        }

        public static object[] getAlgorithms()
        {
            return new object[]
            {
                "BTH",
                "Histogram brzegowy",
                //"Rot + Histogram brzegowy",
                "Progowanie adaptacyjne"
            };
        }

        public SegmentationAlgorithm()
        {
            plateLetters = new List<List<Bitmap>>();
            showOff = false;
        }
        
        public void process(IEnumerable<Bitmap> plates)
        {
            plateLetters = new List<List<Bitmap>>();
            processSegmentation(plates);
            //plateLetters.RemoveAll(plate => plate.Count < 4);
            for (int i = 0; i < plateLetters.Count; i++)
            {
                if (plateLetters[i].Count < 4)
                {
                    plateLetters[i] = null;
                }
                
            }
        }

        protected abstract void processSegmentation(IEnumerable<Bitmap> plates);

        protected void cropAndSelectSigns(Blob[] plateBlobs, Bitmap candidateToPlate, Bitmap processingImage)
        {
            double avg_height = 0;
            int counter = 0;
            foreach (var blob in plateBlobs)
            {
                if (blob.Rectangle.Height > candidateToPlate.Height / 3 && blob.Rectangle.Width < candidateToPlate.Width / 4)
                {
                    avg_height += blob.Rectangle.Height;
                    ++counter;
                }
            }
            avg_height = avg_height / counter;

            var g = Graphics.FromImage(processingImage);
            var lista_liter = new List<Bitmap>();
            
            // sortowanie tablicy po X, aby w dobrej kolejnosci rozpoznac litery
            plateBlobs = plateBlobs.OrderBy(item => item.Rectangle.X).ToArray();

            foreach (Blob b2 in plateBlobs)
            {
                if (b2.Rectangle.Height > candidateToPlate.Height / 3 && b2.Rectangle.Width < candidateToPlate.Width / 4)
                {
                    if (b2.Rectangle.Height / avg_height > 0.85 && b2.Rectangle.Height / avg_height < 1.15)
                    {
                        var wycinanie = new Crop(b2.Rectangle);
                        lista_liter.Add(wycinanie.Apply(candidateToPlate));
                        int left = b2.Rectangle.Left;
                        int top = b2.Rectangle.Top;
                        int width = b2.Rectangle.Width;
                        int height = b2.Rectangle.Height;
                        g.DrawRectangle(new Pen(Color.Red), left, top, width, height);
                    }
                }
            }
            plateLetters.Add(new List<Bitmap>(lista_liter));
        }

        protected bool countSigns(Bitmap candidateToPlate, ref Bitmap processingImage, out Blob[] plateBlobs)
        {
            var blobCounter = new BlobCounter(processingImage);
            plateBlobs = blobCounter.GetObjectsInformation();
            processingImage = new Bitmap(candidateToPlate);
            var licznik = 0;

            if (plateBlobs.Length < 4) // za malo znakow
            {
                return true;
            }
            foreach (var blob in plateBlobs)
            {
                if (blob.Rectangle.Height > candidateToPlate.Height / 3 && blob.Rectangle.Width < candidateToPlate.Width / 4)
                {
                    licznik++;
                }
            }
            if (licznik > 9) // za duzo znakow
            {
                return true;
            }
            return false;
        }

        // TODO : need to write
        protected void ProstowanieTablic(List<Bitmap> probably_plates)
        {
            // dla kazdej zlokalizowanej tablicy :
            var obrocone = new List<Bitmap>();

            foreach (var plate in probably_plates)
            {
                var processingImage = FilteringTool.getBlackTopHat(plate);
                var filters = new FiltersSequence(
                    new SISThreshold(),
                    new Erosion(new short[,] { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 1, 0 } }));
                processingImage = filters.Apply(processingImage);

                Blob[] blobsOnPlate;
                if (countSigns(plate, ref processingImage, out blobsOnPlate)) return;

                #region Wyznaczenie_Rot
                var avg_height = 0;
                var licznik = 0;
                foreach (Blob blob in blobsOnPlate)
                {
                    if (blob.Rectangle.Height > plate.Height / 3 && blob.Rectangle.Width < plate.Width / 4)
                    {
                        avg_height += blob.Rectangle.Height;
                        licznik++;
                    }
                }
                avg_height = avg_height / licznik;

                blobsOnPlate = blobsOnPlate.OrderBy(item => item.Rectangle.X).ToArray();
                var obszary = new List<Blob>();

                foreach (var blob in blobsOnPlate)
                {
                    if (blob.Rectangle.Height > plate.Height / 3 && blob.Rectangle.Width < plate.Width / 4)
                    {
                        if (blob.Rectangle.Height / avg_height > 0.85 && blob.Rectangle.Height / avg_height < 1.15)
                        {
                            obszary.Add(new Blob(blob));
                        }
                    }
                }

                if (obszary.Count > 0)
                {
                    double del_X = (-1 * obszary[0].Rectangle.X + obszary[obszary.Count - 1].Rectangle.X);
                    double del_Y = (-1 * obszary[0].Rectangle.Y + obszary[obszary.Count - 1].Rectangle.Y);
                    var radian = Math.Atan2(del_Y, del_X);
                    var angle = radian * (180 / Math.PI);
                    var rotacja = new RotateBicubic(angle) { KeepSize = false };
                    try
                    {
                        obrocone.Add(rotacja.Apply(plate));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                #endregion
            }

            probably_plates = new List<Bitmap>(obrocone);

            if (showOff)
            {
                foreach (Bitmap test in probably_plates)
                {
                    FilteringTool.showEfects(test, "test");
                }
            }
        }
    }
}