using System;

namespace ProjektMagisterski.Segmentation
{
    public static class SegmentationFactory
    {
        public static SegmentationAlgorithm create(string type)
        {
            SegmentationAlgorithm result = null;

            switch (findSpecifiedAlgorithm(type))
            {
                case 0: result = new SegmentationUsingBTH();
                    break;
                case 1: result = new SegmentationUsingMarginalDistribution();
                    break;
                case 2: result = new SegmentationUsingCharacters();
                    break;
            }
            if (result == null)
            {
                throw new Exception("Algorytm nie dodany do fabryki!");
            }
            return result;
        }

        private static int findSpecifiedAlgorithm(string name)
        {
            var data = SegmentationAlgorithm.getAlgorithms();
            for (var i = 0; i < data.Length; i++)
            {
                if ((string)data[i] == name)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}