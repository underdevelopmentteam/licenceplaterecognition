using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Segmentation
{
    public class SegmentationUsingMarginalDistribution : SegmentationAlgorithm
    {
        protected override void processSegmentation(IEnumerable<Bitmap> plates)
        {
            foreach (var plateCandidate in plates)
            {
                var temp = new Bitmap(plateCandidate);
                temp = Grayscale.CommonAlgorithms.BT709.Apply(temp);

                temp = new FiltersSequence(
                    new SISThreshold(),
                    new Invert(),
                    new BlobsFiltering
                    {
                        MinHeight = plateCandidate.Size.Height/3,
                        MinWidth = 2,
                        MaxWidth = plateCandidate.Size.Width/5
                    },
                    new Invert()).Apply(temp);

                if (showOff) FilteringTool.showEfects(new Bitmap(temp), "Filtracja szumu");

                #region RzutJasnosciIPrzerwy
                var vert = new HorizontalIntensityStatistics(temp);
                var hist = vert.Gray;
                var wartosci = hist.Values;
                var max = hist.Values.Max();

                for (var i = 0; i < wartosci.Length; ++i)
                {
                    if (wartosci[i] < 90 * max / 100)
                    {
                        wartosci[i] = 0;
                    }
                    else
                    {
                        if (wartosci[i] > 98 * max / 100)
                        {
                            wartosci[i] = max;
                        }
                    }
                }
                wartosci[0] = wartosci[0] ;

                var punkty_x_start = new List<int>();
                var punkty_x_koniec = new List<int>();
                var licznik = 0;
                for (int x = 0; x < wartosci.Length-1; ++x)
                {
                    if (wartosci[x] > 0 && wartosci[x + 1] == 0 )
                    {
                        licznik++;
                        punkty_x_start.Add(x);
                    }
                    if (wartosci[x] == 0 && wartosci[x + 1] > 0 && punkty_x_start.Count > 0)
                    {
                        licznik++;
                        punkty_x_koniec.Add(x + 1);
                    }
                }
                #endregion

                #region Wycinanie_i_zaznaczanie_liter

                var lista_liter = new List<Bitmap>();
                var temp2 = new Bitmap(temp);
                temp = new Bitmap(plateCandidate);
                Graphics g = Graphics.FromImage(temp);
                for(int i=0; i< Math.Min(punkty_x_start.Count,punkty_x_koniec.Count); ++i)
                {
                    var figura = new Rectangle(punkty_x_start[i], 0, punkty_x_koniec[i] - punkty_x_start[i], plateCandidate.Size.Height);
                    if (figura.Width < 0)
                    {
                        continue;
                    }
                    var wycinanie = new Crop(figura);
                    
                    #region UcinanieGoryDolu
                    // uciecie gory i dolu :
                    Bitmap ucinacz = wycinanie.Apply(temp2);
                    ucinacz = Grayscale.CommonAlgorithms.BT709.Apply(ucinacz);
                    var sisTH = new SISThreshold();
                    sisTH.ApplyInPlace(ucinacz);

                    var pion = new VerticalIntensityStatistics(ucinacz);
                    Histogram pion_histogram = pion.Gray;
                    int[] wartosci_pion = pion_histogram.Values;
                    

                    int dol = plateCandidate.Height, gora = 0;
                    if (wartosci_pion[wartosci_pion.Length / 2] != 0)
                    {
                        for (int x = wartosci_pion.Length / 2; x < wartosci_pion.Length; ++x)
                        {
                            if (wartosci_pion[x] >= 255* (95*ucinacz.Width/100))
                            {
                                dol = x;
                                break;
                            }
                        }
                        for (int x = wartosci_pion.Length / 2; x >= 0; --x)
                        {
                            if (wartosci_pion[x] >= 255 * (95*ucinacz.Width/100))
                            {
                                gora = x;
                                break;
                            }
                        }
                    }
                    figura = new Rectangle(punkty_x_start[i], gora, punkty_x_koniec[i] - punkty_x_start[i], dol - gora);
                    wycinanie = new Crop(figura);
                    if (figura.Height > 0 && figura.Width > 0)
                    {
                        lista_liter.Add(wycinanie.Apply(plateCandidate));
                    }
                    #endregion

                    int left = punkty_x_start[i];
                    int top = gora;
                    int width = punkty_x_koniec[i] - punkty_x_start[i];
                    int height = dol-gora;
                    g.DrawRectangle(new Pen(Color.Red), left, top, width, height);
                }
                plateLetters.Add(new List<Bitmap>(lista_liter));
                #endregion
                if (showOff) FilteringTool.showEfects(temp, "test");
            }
        }
    }
}