﻿using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Segmentation
{
    public class SegmentationUsingCharacters : SegmentationAlgorithm
    {
        protected override void processSegmentation(IEnumerable<Bitmap> plates)
        {
            foreach (Bitmap plate in plates)
            {
                var processingImage = new FiltersSequence(
                    new GrayscaleBT709(),
                    new BradleyLocalThresholding { WindowSize = plate.Size.Width / 10 },
                    new Invert()).Apply(plate);

                if (showOff) FilteringTool.showEfects(new Bitmap(processingImage), "After BradleyLocalThresholding");

                Blob[] plateBlobs;
                if (countSigns(plate, ref processingImage, out plateBlobs)) return;

                cropAndSelectSigns(plateBlobs, plate, processingImage);

                if (showOff) FilteringTool.showEfects(new Bitmap(processingImage), "Wynik");
            }
        }
    }
}