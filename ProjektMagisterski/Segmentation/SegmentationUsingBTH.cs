using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using ProjektMagisterski.Tools;

namespace ProjektMagisterski.Segmentation
{
    public class SegmentationUsingBTH : SegmentationAlgorithm
    {
        protected override void processSegmentation(IEnumerable<Bitmap> plates)
        {
            foreach (var candidateToPlate in plates)
            {
                var processingImage = FilteringTool.getBlackTopHat(candidateToPlate);

                if (showOff) FilteringTool.showEfects(processingImage, "after BTH");

                processingImage = new FiltersSequence(
                    new SISThreshold(),
                    new Erosion(new short[,] { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 1, 0 } })).Apply(processingImage);

                if (showOff) FilteringTool.showEfects(processingImage, "after SIS and Erosion");

                Blob[] plateBlobs;
                if (countSigns(candidateToPlate, ref processingImage, out plateBlobs)) return; // TODO : merge count signs z cropandselectSigns


                cropAndSelectSigns(plateBlobs, candidateToPlate, processingImage);

                if (showOff) FilteringTool.showEfects(processingImage, "Wynik");
            }
        }
    }
}