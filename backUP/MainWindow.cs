﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace ProjektMagisterski
{
    public partial class MainWindow : Form
    {
        #region PrivateFields
        ObrazDoRozpoznania rozpoznawanie;
        readonly RozpoznawanieLiter req;
        readonly Stopwatch zegarek = new Stopwatch();
        #endregion

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();

            sourcePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            sourcePictureBox.Update();
            sourcePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            sourcePictureBox.Update();

            comboBox1.Items.Add("morfologicznie");
            comboBox1.Items.Add("wlasny");
            comboBox1.Items.Add("wlasny_canny");
            comboBox1.Items.Add("adaptacyjne");
            comboBox1.SelectedIndex = 0;

            comboBox2.Items.Add("BTH");
            comboBox2.Items.Add("RzutJasnosci");
            comboBox2.Items.Add("Rot + RzutJasnosci");
            comboBox2.Items.Add("ProgowanieAdaptacyjne");
            comboBox2.SelectedIndex = 0;

            comboBox3.Items.Add("OCR - rzut jasnosci");
            comboBox3.Items.Add("OCR - szablony");
            comboBox3.Items.Add("OCR - SSN");
            comboBox3.Items.Add("OCR - templates");
            comboBox3.SelectedIndex = 0;

            // wczytanie danych z liter
            req = new RozpoznawanieLiter();
        }
        #endregion

        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            var Okno_wczytywania_pliku = new OpenFileDialog();
            if (Okno_wczytywania_pliku.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    rozpoznawanie = new ObrazDoRozpoznania(Okno_wczytywania_pliku.FileName);
                    sourcePictureBox.Image = rozpoznawanie.getImage();
                    sourcePictureBox.Update();
                    sourcePictureBox.Image = rozpoznawanie.getOriginal();
                    sourcePictureBox.Update();
                    groupBoxAlgorithms.Enabled = true;
                }
                catch
                {
                    MessageBox.Show(string.Format("Zły format pliku!"));
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (rozpoznawanie == null) return;

            try
            {
                rozpoznawanie.reset();
                sourcePictureBox.Image = rozpoznawanie.getImage();
                sourcePictureBox.Update();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void checkBoxShowSteps_CheckedChanged(object sender, EventArgs e)
        {
            if (rozpoznawanie != null)
            {
                rozpoznawanie.show_off = checkBoxShowSteps.Checked;                
            }
            else
            {
                checkBoxShowSteps.Checked = false;
            }
        }

        private void btnLocalize_Click(object sender, EventArgs e)
        {
            FilteringTool.showEfects(null, "reset");

            if (groupBoxAlgorithms.Enabled == false) return;

            rozpoznawanie.show_off = checkBoxShowSteps.Checked;
            rozpoznawanie.reset();
            
            zegarek.Restart();
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    rozpoznawanie.localizeUsingMorphology();
                    break;
                case 1:
                    rozpoznawanie.localizeUsingGaussian();
                    break;
                case 2:
                    rozpoznawanie.localizeUsingCanny();
                    break;
                case 3:
                    rozpoznawanie.localizeUsingAdaptativeThresholding();
                    break;
            }
            zegarek.Stop();

            textBox1.Text = string.Format("{0}ms", zegarek.ElapsedMilliseconds);
            sourcePictureBox.Image = rozpoznawanie.getImage();
            sourcePictureBox.Update();
        }

        private void btnExtract_Click(object sender, EventArgs e)
        {
            FilteringTool.showEfects(null, "reset");

            if (groupBoxAlgorithms.Enabled == false) return;
            rozpoznawanie.show_off = checkBoxShowSteps.Checked;
            rozpoznawanie.resetLetters();
            zegarek.Restart();
            switch (comboBox2.SelectedIndex)
            {
            case 0:
                    rozpoznawanie.segmentacja_BlackTopHat();
                    break;
                case 1:
                    rozpoznawanie.segmentacja_Rzut_jasnosci();
                    break;
                case 2:
                    rozpoznawanie.ProstowanieTablic();
                    rozpoznawanie.segmentacja_Rzut_jasnosci();
                    break;
                case 3:
                    rozpoznawanie.segmentacja_adaptacyjna();
                    break;
            }
            zegarek.Stop();
            textBox2.Text = string.Format("{0}ms", zegarek.ElapsedMilliseconds);

            var nrOfLetters = rozpoznawanie.getLetters().Sum(plate => plate.Count);

            textBox4.Text = string.Format("Z={0}, T={1}", nrOfLetters, rozpoznawanie.getLetters().Count);
        }

        private void btnRecognize_Click(object sender, EventArgs e)
        {
            FilteringTool.showEfects(null, "reset");

            // pokazanie liter:
            var napisy = new List<string>();
            listBoxResults.Items.Clear();

            zegarek.Restart();
            switch (comboBox3.SelectedIndex)
            {
                case 0:
                    napisy = new List<string>(req.rozpoznanie_liter_rzuty(rozpoznawanie.getLetters()));
                    break;
                case 1:
                    napisy = new List<string>(req.rozpoznanie_liter_gestosci(rozpoznawanie.getLetters()));
                    break;
                case 2:
                    napisy = new List<string>(req.rozpoznanie_liter_SSN(rozpoznawanie.getLetters()));
                    break;
                case 3:
                    napisy = new List<string>(req.rozpoznanie_liter_wzorce(rozpoznawanie.getLetters()));
                    break;
            }
            zegarek.Stop();
            textBox3.Text = string.Format("{0}ms", zegarek.ElapsedMilliseconds);

            foreach (var txt in napisy)
            {
                listBoxResults.Items.Add(txt);
            }
        }
    }
}
