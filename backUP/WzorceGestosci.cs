using System;
using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging.Filters;

namespace ProjektMagisterski
{
    public class WzorceGestosci
    {
        private readonly List<List<int>> Wzorce_gestosci = new List<List<int>>();

        public WzorceGestosci()
        {
            WczytajGestosci();
        }

        private void WczytajGestosci()
        {
            const string dicrectory = "WzorceGestosci\\";
            string path;

            for (var x = '0'; x <= '9'; ++x)
            {
                path = dicrectory + x + ".txt";
                Wzorce_gestosci.Add(CommonTools.WczytajDaneZPliku(path));
            }

            for (var i = 'a'; i <= 'z'; ++i)
            {
                path = dicrectory + i + ".txt";
                Wzorce_gestosci.Add(CommonTools.WczytajDaneZPliku(path));
            }
        }

        private int[] gestosci_obrazu(Bitmap litera)
        {
            var letter = Grayscale.CommonAlgorithms.BT709.Apply(litera);

            var wynik = new int[4, 6]; // square 15x15
            for (int a = 0; a < 4; a++) // dla kazdego kwadratu w poziomie
            {
                for (int b = 0; b < 6; b++) // dla kazdego kwadratu w pionie
                {
                    #region DlaKazdegoPixelaWKwadracie
                    for (int y = b * 15; y < 15 * (b + 1); y++)
                    {
                        for (int x = a * 15; x < 15 * (a + 1); x++)
                        {
                            wynik[a, b] += letter.GetPixel(x, y).R / 100;
                        }
                    }
                    #endregion
                }
            }

            var result = new int[24];

            for (var y = 0; y < 6; y++)
            {
                for (var x = 0; x < 4; x++)
                {
                    result[x + y * 4] = wynik[x, y];
                }
            }

            return result;
        }

        private char zaklasyfikuj_litere_gestosci(int[] gestosci)
        {
            var wynik = new double[36];

            for (var numer = 0; numer < 36; ++numer)
            {
                wynik[numer] = 0;
                var aktualne = Wzorce_gestosci[numer];
                for (var i = 0; i < aktualne.Count; ++i)
                {
                    var a = aktualne[i] + 0.01;
                    var b = gestosci[i] + 0.01;
                    var relativity = Math.Min(b/a, a/b);
                    
                    if (relativity > 0.3)
                    {
                        wynik[numer] += relativity;
                    }
                }
            }

            double maxV = 0.0f;
            int index = 0;
            for (int i = 0; i < 36; ++i)
            {
                if (wynik[i] > maxV)
                {
                    maxV = wynik[i];
                    index = i;
                }
            }

            // tutaj po "index" ustalamy wskazana litere.
            char[] tabofresult = new char[36] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            return tabofresult[index];
        }

        public List<string> rozpoznanie_liter_gestosci(List<List<Bitmap>> listy_liter)
        {
            var tekst = new List<string>();

            foreach (var lista in listy_liter)
            {
                var napis = "";
                foreach (var bmp in lista)
                {
                    var temp = zaklasyfikuj_litere_gestosci(gestosci_obrazu(FilteringTool.convertToLetterStandard(bmp)));
                    napis += temp;
                }
                tekst.Add(napis);
            }
            return tekst;
        }
    }
}