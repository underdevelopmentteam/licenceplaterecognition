﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ProjektMagisterski
{
    public partial class ImageViewer : Form
    {
        private List<Bitmap> images= new List<Bitmap>(); 

        public ImageViewer(Bitmap image, String text)
        {
            InitializeComponent();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            addImage(image, text);
            listBox1.SelectedIndex = 0;
        }

        public void addImage(Bitmap image, string text)
        {
            images.Add(new Bitmap(image));
            listBox1.Items.Add(text);
            listBox1.Update();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox1.Image = images[listBox1.SelectedIndex];
            pictureBox1.Update();
            this.Text = (string) listBox1.Items[listBox1.SelectedIndex];
        }
    }
}
