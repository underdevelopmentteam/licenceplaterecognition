﻿using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using AForge.Imaging;
using AForge.Neuro;
using AForge.Neuro.Learning;
using Image = AForge.Imaging.Image;

namespace ProjektMagisterski
{
    public class RozpoznawanieLiter
    {
        int numerek = 0;

        private readonly RzutyJasnosci rzutyJasnosci = new RzutyJasnosci();
        private readonly WzorceGestosci wzorceGestosci = new WzorceGestosci();

        Network network;

        public RozpoznawanieLiter()
        {
            //uczenie_SSN();
            //network = AForge.Neuro.Network.Load("nauczona_siec3");
            
            wczytaj_wzorce_template_match();
        }

        public List<string> rozpoznanie_liter_rzuty(List<List<Bitmap>> listy_liter)
        {
            return rzutyJasnosci.rozpoznanie_liter_rzuty(listy_liter);
        }

        public List<string> rozpoznanie_liter_gestosci(List<List<Bitmap>> listy_liter)
        {
            return wzorceGestosci.rozpoznanie_liter_gestosci(listy_liter);
        }









        // Works 95%
        // wzorce convolution
        List<Bitmap> wzorce = new List<Bitmap>();        
        #region ExhaustiveTemplateMatching
        /// <summary>
        /// Loads templates from files
        /// </summary>
        private void wczytaj_wzorce_template_match()
        {

            string path = "WzorcoweLitery\\";
            char letter = '0';
            for (int i = 0; i < 10; i++)
            {
                string sciezka = path + letter.ToString()+".bmp";
                wzorce.Add(new Bitmap(sciezka));
                letter++;
            }
            letter = 'A';
            for (int i = 0; i < 26; i++)
            {
                string sciezka = path + letter.ToString() + ".bmp";
                wzorce.Add(new Bitmap(sciezka));
                letter++;
                //if (letter == 'Q')
                //{
                //    letter++;
                //    i++;
                //}
            }
        }
        /// <summary>
        /// Compare templates with image and returns the best matching charakter
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        private char porownaj_wzorce(Bitmap image)
        {
            ExhaustiveTemplateMatching template = new ExhaustiveTemplateMatching();
            template.SimilarityThreshold = 0.3f;
            image = image.Clone(new Rectangle(0, 0, image.Size.Width, image.Size.Height), PixelFormat.Format8bppIndexed);
            Bitmap wzorzec = null;
            
            double[] wynik = new double[wzorce.Count];
            
            for (int x = 0; x < 36; x++)
            {
                wzorzec = wzorce[x].Clone(new Rectangle(0, 0, image.Size.Width, image.Size.Height), PixelFormat.Format8bppIndexed);
                var result = template.ProcessImage(image, wzorzec);
                if (result.Length > 0)
                {
                    wynik[x] = (template.ProcessImage(image, wzorzec))[0].Similarity;
                }
                else
                {
                    wynik[x] = 0.0f;
                }
            }

            wynik[26] = 0.0f; // brak Q w tablicach rejestracyjnych
            double max = 0.0f;
            int index_max = 0;
            for (int x = 0; x < 36; x++)
            {
                if (wynik[x] > max)
                {
                    max = wynik[x];
                    index_max = x;
                }
            }

            char[] result_table = new char[36] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            return result_table[index_max];
        }
        #endregion
        /// <summary>
        /// Input list of list of letters  -> process - >Returns recognized licence plates
        /// </summary>
        /// <param name="listy_liter"></param>
        /// <returns></returns>
        public List<string> rozpoznanie_liter_wzorce(List<List<Bitmap>> listy_liter)
        {
            string txt = "";
            List<string> wynik = new List<string>();

            foreach (var item in listy_liter)
            {
                txt = "";
                foreach (var letter in item)
                {
                    txt += porownaj_wzorce(FilteringTool.convertToLetterStandard(letter));
                }
                wynik.Add(txt);
            }
            return wynik;
        }

        // mniej na wejście
        // uczenie sieci neuronowej
        private double[] przerob_obraz_na_wejscie_sieci(Bitmap bmp)
        {
            //Bitmap temp = AForge.Imaging.Filters.Grayscale.CommonAlgorithms.BT709.Apply(bmp);
            Bitmap temp = new Bitmap(bmp);

            int[,] wynik = new int[12, 18]; // square 15x15
            
            for (int a = 0; a < 12; a++) // dla kazdego kwadratu w poziomie
            {
                for (int b = 0; b < 18; b++) // dla kazdego kwadratu w pionie
                {
                    #region DlaKazdegoPixelaWKwadracie
                    for (int y = b * 5; y < 5 * (b + 1); y++)
                    {
                        for (int x = a * 5; x < 5 * (a + 1); x++)
                        {
                            wynik[a, b] += temp.GetPixel(x, y).R / 200;
                        }
                    }
                    #endregion
                }
            }

            double[] wynik1D = new double[12 * 18];
            int index = 0;
            for (int a = 0; a < 12; a++) // dla kazdego kwadratu w poziomie
            {
                for (int b = 0; b < 18; b++) // dla kazdego kwadratu w pionie
                {
                    wynik1D[index++] = wynik[a, b];
                }
            }
            for (int i = 0; i < wynik1D.Length; ++i)
            {
                if (wynik1D[i] < 9)
                {
                    wynik1D[i] = -1;
                }
                if (wynik1D[i] < 18 && wynik1D[i] > 8)
                {
                    wynik1D[i] = 0;
                }
                if (wynik1D[i] >17)
                {
                    wynik1D[i] = 1;
                }
            }
            return wynik1D;
        }
        ActivationNetwork ssn = new ActivationNetwork(new BipolarSigmoidFunction(2), 12 * 18, new int[3] { 48, 40, 36 });
        #region SieciNeuronowe
        // do poprawy :
        private double[] zapisanie_obrazu_1m1(Bitmap bmp)
        {
            //int index =0;
            double[] wynik = przerob_obraz_na_wejscie_sieci(bmp);
            //double[] wynik = new double[90 * 60];
            //bmp = bmp.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //int[] wynik_int= gestosci_obrazu(bmp);
            /*
            for (int i = 0; i < bmp.Size.Height; ++i)
            {
                for (int j = 0; j < bmp.Size.Width; ++j)
                {
                    if (bmp.GetPixel(j, i).R > 100)
                    {
                        wynik[index++] = 1;
                    }
                    else
                    {
                        wynik[index++] = -1;
                    }
                }
            }
            */
            /*
            for (int i = 0; i < 24; ++i)
            {
                wynik[index++] = 2 * (wynik_int[i]) / (450) - 1;
                /*
                if (wynik_int[i] > 15 * 15 * 60 / 100)
                {
                    wynik[index++] = 1.0f;
                }
                else
                {
                    wynik[index++] = 0.0f;
                }
                */
            //}
            
            return wynik;
        }
        private void uczenie_SSN()
        {
            ssn.Randomize();
            //AForge.Neuro.Learning.PerceptronLearning uczenie = new AForge.Neuro.Learning.PerceptronLearning(ssn);
            BackPropagationLearning nauczyciel = new BackPropagationLearning(ssn);
            
            //uczenie.LearningRate = 1;

            double[] znak = new double[37];
            for (int i = 0; i < 37; ++i) znak[i] = -1;  // wszysko na -1 potem 1 w odpowiednim miejscu

            int ilosc_obrazow = 344;
            double[][] wejscie = new double[ilosc_obrazow][];
            for (int i = 0; i < ilosc_obrazow; ++i) wejscie[i] = new double[12*18];
            
            double[][] wyjscie = new double[ilosc_obrazow][];
            for (int i = 0; i < ilosc_obrazow; ++i) wyjscie[i] = new double[37];
            
            int obraz_numer = 0;
            double[] dany_znak = znak;
            znak.CopyTo(dany_znak, 0);

            Bitmap temp;

            #region Letters
                // 0
                string path = "Litery\\0\\";
                dany_znak[0] = 1;
                for(int i = 1; i<24; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    //wyjscie[obraz_numer++] = znak;
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // 1
                path = "Litery\\1\\";
                dany_znak[0] = -1;
                dany_znak[1] = 1;
                for(int i = 1; i<10; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);

                }
                // 2
                path = "Litery\\2\\";
                dany_znak[1] = -1;
                dany_znak[2] = 1;
                for(int i = 1; i<14; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // 3
                path = "Litery\\3\\";
                dany_znak[2] = -1;
                dany_znak[3] = 1;
                for(int i = 1; i<8; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);

                }
                // 4
                path = "Litery\\4\\";
                dany_znak[3] = -1;
                dany_znak[4] = 1;
                for(int i = 1; i<10; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // 5
                path = "Litery\\5\\";
                dany_znak[4] = -1;
                dany_znak[5] = 1;
                for(int i = 1; i<10; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // 6
                path = "Litery\\6\\";
                dany_znak[5] = -1;
                dany_znak[6] = 1;
                for(int i = 1; i<8; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // 7
                path = "Litery\\7\\";
                dany_znak[6] = -1;
                dany_znak[7] = 1;
                for(int i = 1; i<12; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // 8
                path = "Litery\\8\\";
                dany_znak[7] = -1;
                dany_znak[8] = 1;
                for(int i = 1; i<7; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // 9
                path = "Litery\\9\\";
                dany_znak[8] = -1;
                dany_znak[9] = 1;
                for(int i = 1; i<16; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // A
                path = "Litery\\A\\";
                dany_znak[9] = -1;
                dany_znak[10] = 1;
                for(int i = 1; i<16; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // B
                path = "Litery\\B\\";
                dany_znak[10] = -1;
                dany_znak[11] = 1;
                for(int i = 1; i<12; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // C
                path = "Litery\\C\\";
                dany_znak[11] = -1;
                dany_znak[12] = 1;
                for(int i = 1; i<8; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // D
                path = "Litery\\D\\";
                dany_znak[12] = -1;
                dany_znak[13] = 1;
                for(int i = 1; i<17; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // E
                path = "Litery\\E\\";
                dany_znak[13] = -1;
                dany_znak[14] = 1;
                for(int i = 1; i<11; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // F
                path = "Litery\\F\\";
                dany_znak[14] = -1;
                dany_znak[15] = 1;
                for(int i = 1; i<5; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // G
                path = "Litery\\G\\";
                dany_znak[15] = -1;
                dany_znak[16] = 1;
                for(int i = 1; i<2; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // H
                path = "Litery\\H\\";
                dany_znak[16] = -1;
                dany_znak[17] = 1;
                for(int i = 1; i<2; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // I
                path = "Litery\\I\\";
                dany_znak[17] = -1;
                dany_znak[18] = 1;
                for(int i = 1; i<2; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // J
                path = "Litery\\J\\";
                dany_znak[18] = -1;
                dany_znak[19] = 1;
                for(int i = 1; i<2; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);                    
                }
                // K
                path = "Litery\\K\\";
                dany_znak[19] = -1;
                dany_znak[20] = 1;
                for(int i = 1; i<8; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // L
                path = "Litery\\L\\";
                dany_znak[20] = -1;
                dany_znak[21] = 1;
                for(int i = 1; i<13; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);                    
                }
                // M
                path = "Litery\\M\\";
                dany_znak[21] = -1;
                dany_znak[22] = 1;
                for(int i = 1; i<8; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);                    
                }
                // N
                path = "Litery\\N\\";
                dany_znak[22] = -1;
                dany_znak[23] = 1;
                for(int i = 1; i<8; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);                    
                }
                // O
                path = "Litery\\O\\";
                dany_znak[23] = -1;
                dany_znak[24] = 1;
                for(int i = 1; i<19; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);                    
                }
                // P
                path = "Litery\\P\\";
                dany_znak[24] = -1;
                dany_znak[25] = 1;
                for(int i = 1; i<8; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);                    
                }
                // Q
                path = "Litery\\Q\\";
                dany_znak[25] = -1;
                dany_znak[26] = 1;
                for(int i = 1; i<2; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // R
                path = "Litery\\R\\";
                dany_znak[27] = -1;
                dany_znak[28] = 1;
                for(int i = 1; i<9; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // S
                path = "Litery\\S\\";
                dany_znak[28] = -1;
                dany_znak[29] = 1;
                for(int i = 1; i<12; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // T
                path = "Litery\\T\\";
                dany_znak[29] = -1;
                dany_znak[30] = 1;
                for(int i = 1; i<12; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // U
                path = "Litery\\U\\";
                dany_znak[30] = -1;
                dany_znak[31] = 1;
                for(int i = 1; i<5; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // V
                path = "Litery\\V\\";
                dany_znak[31] = -1;
                dany_znak[32] = 1;
                for(int i = 1; i<3; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // W
                path = "Litery\\W\\";
                dany_znak[32] = -1;
                dany_znak[33] = 1;
                for(int i = 1; i<23; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // X
                path = "Litery\\X\\";
                dany_znak[33] = -1;
                dany_znak[34] = 1;
                for(int i = 1; i<9; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // Y
                path = "Litery\\Y\\";
                dany_znak[34] = -1;
                dany_znak[35] = 1;
                for(int i = 1; i<4; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
                // Z
                path = "Litery\\Z\\";
                dany_znak[35] = -1;
                dany_znak[36] = 1;
                for(int i = 1; i<7; ++i)
                {
                    temp = Image.FromFile(path + i.ToString() + ".bmp");
                    wejscie[obraz_numer] = zapisanie_obrazu_1m1(temp);
                    dany_znak.CopyTo(wyjscie[obraz_numer++], 0);
                }
            #endregion

            bool ucz = true;
            int iteracja = 0;

            nauczyciel.LearningRate = 1.00f;
            nauczyciel.Momentum = 0.1f;
            
            while (ucz)
            {
                //double blad = uczenie.RunEpoch(wejscie, wyjscie);
                double blad = nauczyciel.RunEpoch(wejscie, wyjscie);
                //ssn.Save("siec_neuronowa.txt");
                //ssn.Randomize();
                if (iteracja % 30000 == 0)
                {
                    nauczyciel.LearningRate -= 0.05f;
                }
                if (blad < 0.5)
                {
                    ucz = false;
                    ssn.Save("nauczona_siec3");
                }
                else
                {
                    if (iteracja < 2000000)
                    {
                        iteracja++;
                    }
                    else
                    {
                        ucz = false;
                        ssn.Save("nauczona_siec3");
                    }
                }
            }
            

        }
        private char zaklasyfikuj_litere_ssn(double[] wynik)
        {
            // brak Q
            wynik[26] = -4.0f;
            double min = -3.0f;
            int index = 0;
            for (int i = 0; i < 36; ++i)
            {
                if (wynik[i] >min)
                {
                    min = wynik[i];
                    index = i;
                }
            }
            // tutaj po "index" ustalamy wskazana litere.
            char[] tabofresult = new char[36] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            return tabofresult[index];
        }
        #endregion
        public List<string> rozpoznanie_liter_SSN(List<List<Bitmap>> listy_liter)
        {
            List<string> tekst = new List<string>();
            char temp;
            string napis;

            foreach (List<Bitmap> lista in listy_liter)
            {
                napis = "";
                foreach (Bitmap bmp in lista)
                {
                    temp = zaklasyfikuj_litere_ssn(network.Compute(zapisanie_obrazu_1m1(FilteringTool.convertToLetterStandard(bmp))));
                    //temp = zaklasyfikuj_litere_ssn(ssn.Compute(zapisanie_obrazu_1m1(preprocess(bmp))));
                    napis += temp;
                }
                tekst.Add(napis);
            }
            return tekst;
        }
    }
}

