using System.Collections.Generic;
using System.IO;

namespace ProjektMagisterski
{
    public static class CommonTools
    {
        public static List<int> WczytajDaneZPliku(string nazwa)
        {
            var temp = new List<int>();
            var linie = File.ReadAllLines(nazwa);
            foreach (string linia in linie)
            {
                foreach (string napis in linia.Split(' '))
                {
                    if (napis != "")
                    {
                        temp.Add(int.Parse(napis));
                    }
                }
            }
            return new List<int>(temp);
        }
    }
}