﻿using System;
using System.Drawing;
using System.Windows.Forms;
using AForge.Imaging.Filters;

namespace ProjektMagisterski
{
    public static class FilteringTool
    {
        private static ImageViewer resultViewer;

        public static int[,] createPiramidKernel(int rozmiar)
        {
            int[,] kernel = new int[rozmiar, rozmiar];
            int start = 0;
            for (int s = 0; s < rozmiar / 2; ++s)
            {
                for (int y = start; y < rozmiar - start; ++y)
                {
                    for (int x = start; x < rozmiar - start; ++x)
                    {
                        kernel[y, x] = start * 2 + 1;
                    }
                }
                start++;
            }
            return kernel;
        }

        public static void showEfects(Bitmap image, String text)
        {
            if (text == "reset")
            {
                resultViewer = null;
                return;
            }

            if (resultViewer == null)
            {
                resultViewer = new ImageViewer(image, text);
                resultViewer.Show();
            }
            else
            {
                resultViewer.addImage(image, text);
            }
        }

        public static void verticalOpening(Bitmap p_image)
        {
            // 1%
            var dyl_x = new short[,] { { -1, 1, -1 }, { -1, 1, -1 }, { -1, 1, -1 } };
            var dylatacja = new Dilatation(dyl_x);
            var erozja = new Erosion(dyl_x);

            for (int x = 0; x < p_image.Height * 1 / 100; ++x)
            {
                erozja.ApplyInPlace(p_image);
            }
            for (int x = 0; x < p_image.Height * 1 / 100; ++x)
            {
                dylatacja.ApplyInPlace(p_image);
            }
        }

        public static void horizontalOpening(Bitmap p_image)
        {
            // 3%
            var dyl_x = new short[,] { { -1, -1, -1 }, { 1, 1, 1 }, { -1, -1, -1 } };
            var dylatacja = new Dilatation(dyl_x);
            for (int x = 0; x < p_image.Width * 3 / 100; ++x)
            {
                dylatacja.ApplyInPlace(p_image);
            }
            var erozja = new Erosion(dyl_x);
            for (int x = 0; x < p_image.Width * 3 / 100; ++x)
            {
                erozja.ApplyInPlace(p_image);
            }
        }

        public static int findMaxRedValue(Bitmap p_image)
        {
            int max_v = 0;
            for (int y = 0; y < p_image.Height; ++y)
            {
                for (int x = 0; x < p_image.Width; ++x)
                {
                    if (p_image.GetPixel(x, y).R > max_v)
                    {
                        max_v = p_image.GetPixel(x, y).R;
                    }
                }
            }
            return max_v;
        }

        public static Bitmap getBlackTopHat(Bitmap p_image)
        {
            var dylatacja = new Dilatation();
            var erozja = new Erosion();

            var temp = new Bitmap(p_image);
            temp = Grayscale.CommonAlgorithms.BT709.Apply(temp);
            var temp2 = new Bitmap(temp);
            temp2 = Grayscale.CommonAlgorithms.BT709.Apply(temp2);
            dylatacja.ApplyInPlace(temp);
            dylatacja.ApplyInPlace(temp);
            dylatacja.ApplyInPlace(temp);
            erozja.ApplyInPlace(temp);
            erozja.ApplyInPlace(temp);
            erozja.ApplyInPlace(temp);
            var odejmowanie = new Subtract(temp2);
            odejmowanie.ApplyInPlace(temp);
            return temp;
        }

        public static Bitmap convertToLetterStandard(Bitmap letter)
        {
            var fs = new FiltersSequence
            {
                new GrayscaleBT709(),
                new SISThreshold(),
                new ResizeBicubic(60, 90)
            };

            try
            {
                letter = fs.Apply(letter);
            }
            catch (Exception)
            {
                MessageBox.Show(string.Format("Cannot convert image"));
            }

            return new Bitmap(letter);
        }
    }
}