﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math;

namespace ProjektMagisterski
{
    public class ObrazDoRozpoznania
    {
        public bool show_off;

        #region PrivateFields
        private Bitmap Obrazek { get; set; }
        private Bitmap Oryginal { get; set; }
        private List<Bitmap> probably_plates = new List<Bitmap>();
        private List<List<Bitmap>> plate_letters = new List<List<Bitmap>>();
        #endregion

        #region Constructor
        public ObrazDoRozpoznania(string obraz)
        {
            Obrazek = new Bitmap(obraz);
            Oryginal = Obrazek;
            Obrazek = Grayscale.CommonAlgorithms.BT709.Apply(Obrazek);
            probably_plates = new List<Bitmap>();
        }
        #endregion

        #region HelperFunctions
        private void printPlatesOnImage(IEnumerable<Blob> wynik)
        {
            Obrazek = new Bitmap(Oryginal);
            Graphics g = Graphics.FromImage(Obrazek);
            foreach (Blob b in wynik)
            {
                int left = b.Rectangle.Left - 2 * Obrazek.Width / 100;
                int top = b.Rectangle.Top - 2 * Obrazek.Height / 100;
                int width = b.Rectangle.Width + 4 * Obrazek.Width / 100;
                int height = b.Rectangle.Height + 4 * Obrazek.Height / 100;
                if (left < 0) left = 0;
                if (top < 0) top = 0;
                g.DrawRectangle(new Pen(Color.Red), left, top, width, height);
                var wycinanie = new Crop(new Rectangle(left, top, width, height));
                probably_plates.Add(wycinanie.Apply(Oryginal));
            }
        }

        private void connectNearbyBlobs(IReadOnlyList<Blob> wynik)
        {
            Obrazek = new Bitmap(Oryginal.Width, Oryginal.Height);
            Graphics g1 = Graphics.FromImage(Obrazek);
            g1.Clear(Color.Black);

            for (int y = 0; y < wynik.Count; y++)
            {
                for (int x = 0; x < wynik.Count; x++)
                {
                    if (x != y)
                    {
                        int odl_x = Math.Abs(wynik[x].Rectangle.X - wynik[y].Rectangle.X);
                        int odl_y = Math.Abs(wynik[x].Rectangle.Y - wynik[y].Rectangle.Y);

                        if (odl_x < 5 * wynik[y].Rectangle.Width && odl_y < 3 * Obrazek.Height / 100)
                        {
                            //zgodnosc wysokosci znakow :
                            var stosunek = wynik[y].Rectangle.Height / wynik[x].Rectangle.Height;
                            if (stosunek > 0.75f && stosunek < 1.25f)
                            {
                                // rysujemy polaczenie
                                g1.DrawLine(new Pen(Color.White), new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y),
                                    new Point(wynik[y].Rectangle.X + wynik[y].Rectangle.Width,
                                        wynik[y].Rectangle.Y + wynik[y].Rectangle.Height));
                                g1.DrawLine(new Pen(Color.White), new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y - 1),
                                    new Point(wynik[y].Rectangle.X + wynik[y].Rectangle.Width,
                                        wynik[y].Rectangle.Y - 1 + wynik[y].Rectangle.Height));
                                g1.DrawLine(new Pen(Color.White), new Point(wynik[x].Rectangle.X, wynik[x].Rectangle.Y + 1),
                                    new Point(wynik[y].Rectangle.X + wynik[y].Rectangle.Width,
                                        wynik[y].Rectangle.Y - 2 + wynik[y].Rectangle.Height));
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region AlgorytmyLokalizacjiTablic

        public void localizeUsingMorphology()
        {
            Obrazek = FilteringTool.getBlackTopHat(Obrazek);

            if( show_off ) FilteringTool.showEfects(new Bitmap(Obrazek), "blackTopHat");

            FilteringTool.horizontalOpening(Obrazek);
            FilteringTool.verticalOpening(Obrazek);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "AfterMorphology");

            // progowanie :
            var max_v = FilteringTool.findMaxRedValue(Obrazek);
            var threshold = new Threshold(max_v/2);
            threshold.ApplyInPlace(Obrazek);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Po progowaniu");

            // Filtracja za malych obszarow o zlych proporcjach
            BlobCounterBase bc = new BlobCounter(Obrazek);
            Blob[] blobs = bc.GetObjectsInformation();

            var wynik = new List<Blob>();
            foreach (var b in blobs)
            {
                if (b.Rectangle.Width > 2*b.Rectangle.Height )
                {
                    if (b.Area > 16 * Obrazek.Height / 100 * Obrazek.Width / 100)
                    {
                        wynik.Add(b);
                    }
                }
            }

            printPlatesOnImage(wynik);
        }

        public void localizeUsingGaussian()
        {
            // 1.  filtr medianowy, gaussa
            var filterSequence = new FiltersSequence( new Median(), new GaussianBlur());
            Obrazek = filterSequence.Apply(Obrazek);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Po filtrze medianowym i gausa 3x3");

            // 3. filtr laplace'a
            int[,] matrix = { { 0, -1, 0 }, { -1, 4, -1 }, { 0, -1, 0 } };
            var laplasjan = new Convolution(matrix);
            laplasjan.ApplyInPlace(Obrazek);
            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Po filtrze Laplace'a");
            
            // 4. filtr sobela - pionowe krawedzie
            matrix = new[,] { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
            laplasjan = new Convolution(matrix);
            laplasjan.ApplyInPlace(Obrazek);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Przed filtrem gestosciowym");

            // 5. Własny filtr "gestosciowy"
            int rozmiar = 3* Obrazek.Width / 100;
            if (rozmiar % 2 == 0) rozmiar++;
            laplasjan = new Convolution(FilteringTool.createPiramidKernel(rozmiar));
            laplasjan.ApplyInPlace(Obrazek);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Po filtrze gestosciowym");
            
            // 6. Binearyzacja metoda SIS - na podstawie histogramu
            var sisThreshold = new SISThreshold();
            sisThreshold.ApplyInPlace(Obrazek);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Po progowaniu");

            // 7. Filtracja za malych obszarow o zlych proporcjach
            BlobCounterBase bc = new BlobCounter(Obrazek);
            Blob[] blobs = bc.GetObjectsInformation();
            var wynik = new List<Blob>();

            foreach (Blob b in blobs)
            {
                if (b.Rectangle.Width > 2.5 * b.Rectangle.Height && b.Rectangle.Width < b.Rectangle.Height * 10)
                {
                    if (b.Area > 16 * Obrazek.Height / 100 * Obrazek.Width / 100)
                    {
                        wynik.Add(b);
                    }
                }
            }

            printPlatesOnImage(wynik);
        }

        public void localizeUsingCanny()
        {
            // 1. Obliczenie H&L thresholds
            var sisThreshold = new SISThreshold();
            int threshold = sisThreshold.CalculateThreshold(Obrazek, new Rectangle(0, 0, Obrazek.Width, Obrazek.Height));
            
            // 2. Canny:
            var canny = new CannyEdgeDetector
            {
                LowThreshold = Convert.ToByte(8*threshold/10),
                HighThreshold = Convert.ToByte(threshold/2)
            };
            canny.ApplyInPlace(Obrazek);

            // 3. Dylatacja
            var dylatacja = new Dilatation();
            dylatacja.ApplyInPlace(Obrazek);
            
            if(show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "po canny");
            
            // 4. Filtracja za malych obszarow
            var BlobFilter = new BlobsFiltering
            {
                MinWidth = 2*Obrazek.Width/100,
                MinHeight = 1*Obrazek.Width/100,
                MaxWidth = 50*Obrazek.Width/100,
                MaxHeight = 40*Obrazek.Height/100
            };
            BlobFilter.ApplyInPlace(Obrazek);
            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "po canny");

            // 5. filtracja blobsow :
            BlobCounterBase bc = new BlobCounter(Obrazek);
            Blob[] blobs = bc.GetObjectsInformation();
            var wynik = new List<Blob>();

            foreach (Blob b in blobs)
            {
                if (b.Rectangle.Width > 2.2f * b.Rectangle.Height && b.Rectangle.Height>Obrazek.Height/100)
                {
                    if (b.Area > 16 * Obrazek.Height / 100 * Obrazek.Width / 100)
                    {
                        float brightness = b.ColorMean.GetBrightness();
                        byte meanRed = b.ColorMean.R;

                        if (brightness > 0.35 && meanRed > 110 &&
                            b.Fullness > 0.1f && b.Fullness < 0.9f)
                        {
                            wynik.Add(b);
                        }
                    }
                }
            }

            printPlatesOnImage(wynik);
        }
        
        public void localizeUsingAdaptativeThresholding()
        {
            // 1. Progowanie adaptacyjne :
            var adaptativeThresholding = new BradleyLocalThresholding {WindowSize = Obrazek.Width/10};
            adaptativeThresholding.ApplyInPlace(Obrazek);

            // 2. Odwrocenie kolorow :
            var invert = new Invert();
            invert.ApplyInPlace(Obrazek);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Progowanie");

            // 3. filtracja blobsow ze wzgledu na ksztalt liter:
            BlobCounterBase bc = new BlobCounter(Obrazek);
            Blob[] blobs = bc.GetObjectsInformation();
            var wynik = new List<Blob>();
            foreach (Blob b in blobs)
            {
                // max sizes
                if (b.Rectangle.Width < Obrazek.Width/20 && b.Rectangle.Width < b.Rectangle.Height &&
                    b.Rectangle.Height > Obrazek.Height / 100 && b.Fullness > 0.2f)
                {
                    // max surface :
                    if (b.Rectangle.Width*b.Rectangle.Height < Obrazek.Width*Obrazek.Width/100) // 10%x30% WxH
                    {
                        if (b.Rectangle.Width*b.Rectangle.Height > Obrazek.Width/100*Obrazek.Width/100)
                        {
                            wynik.Add(b);
                        }
                        else if (3*b.Rectangle.Width < b.Rectangle.Height)
                        {
                            wynik.Add(b);
                        }
                    }

                }
            }
            // 4. Laczenie nie odleglych obszarow :
            connectNearbyBlobs(wynik);

            if (show_off) FilteringTool.showEfects(new Bitmap(Obrazek), "Linie laczace ~litery");
            
            // 5. Filtrowanie obszarów :
            BlobCounterBase bc2 = new BlobCounter(Obrazek);
            Blob[] obszary = bc2.GetObjectsInformation();
            wynik = new List<Blob>();

            foreach (Blob b in obszary)
            {
                if (b.Rectangle.Width > 2*b.Rectangle.Height && b.Fullness > 0.2f )
                {
                    if (b.Rectangle.Width*b.Rectangle.Height > Obrazek.Height*Obrazek.Width/500)
                    {
                        wynik.Add(b);
                    }
                }
            }

            // 6. Zaznaczanie tablic
            printPlatesOnImage(wynik);
        }

        #endregion

        #region AlgorytmySegmentacji
        private void cropAndSelectSigns(Blob[] plateBlobs, Bitmap candidateToPlate, Bitmap processingImage)
        {
            double avg_height = 0;
            int counter = 0;
            foreach (var blob in plateBlobs)
            {
                if (blob.Rectangle.Height > candidateToPlate.Height / 3 && blob.Rectangle.Width < candidateToPlate.Width / 4)
                {
                    avg_height += blob.Rectangle.Height;
                    ++counter;
                }
            }
            avg_height = avg_height / counter;

            var g = Graphics.FromImage(processingImage);
            var lista_liter = new List<Bitmap>();
            // sortowanie tablicy po X, aby w dobrej kolejnosci rozpoznac litery
            plateBlobs = plateBlobs.OrderBy(item => item.Rectangle.X).ToArray();

            foreach (Blob b2 in plateBlobs)
            {
                if (b2.Rectangle.Height > candidateToPlate.Height / 3 && b2.Rectangle.Width < candidateToPlate.Width / 4)
                {
                    if (b2.Rectangle.Height / avg_height > 0.85 && b2.Rectangle.Height / avg_height < 1.15)
                    {
                        var wycinanie = new Crop(b2.Rectangle);
                        lista_liter.Add(wycinanie.Apply(candidateToPlate));
                        int left = b2.Rectangle.Left;
                        int top = b2.Rectangle.Top;
                        int width = b2.Rectangle.Width;
                        int height = b2.Rectangle.Height;
                        g.DrawRectangle(new Pen(Color.Red), left, top, width, height);
                    }
                }
            }
            plate_letters.Add(new List<Bitmap>(lista_liter));
        }

        private static bool countSigns(Bitmap candidateToPlate, ref Bitmap processingImage, out Blob[] plateBlobs)
        {
            var blobCounter = new BlobCounter(processingImage);
            plateBlobs = blobCounter.GetObjectsInformation();
            processingImage = new Bitmap(candidateToPlate);
            var licznik = 0;

            if (plateBlobs.Length < 4) // za malo znakow
            {
                return true;
            }
            foreach (var blob in plateBlobs)
            {
                if (blob.Rectangle.Height > candidateToPlate.Height / 3 && blob.Rectangle.Width < candidateToPlate.Width / 4)
                {
                    licznik++;
                }
            }
            if (licznik > 9) // za duzo znakow
            {
                return true;
            }
            return false;
        }        

        public void segmentacja_BlackTopHat()
        {
            foreach (var candidateToPlate in probably_plates)
            {
                var processingImage = FilteringTool.getBlackTopHat(candidateToPlate);

                if (show_off) FilteringTool.showEfects(processingImage, "after BTH");
                
                processingImage = new FiltersSequence(
                    new SISThreshold(),
                    new Erosion(new short[,] { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 1, 0 } })).Apply(processingImage);    
                
                if (show_off) FilteringTool.showEfects(processingImage, "after SIS and Erosion");

                Blob[] plateBlobs;
                if (countSigns(candidateToPlate, ref processingImage, out plateBlobs)) return;

                cropAndSelectSigns(plateBlobs, candidateToPlate, processingImage);

                if(show_off)FilteringTool.showEfects(processingImage, "Wynik");
            }
        }

        public void segmentacja_adaptacyjna()
        {
            foreach (Bitmap plate in probably_plates)
            {
                var processingImage = new FiltersSequence(
                    new GrayscaleBT709(),
                    new BradleyLocalThresholding { WindowSize = plate.Size.Width / 10 },
                    new Invert()).Apply(plate);

                if (show_off) FilteringTool.showEfects(new Bitmap(processingImage), "After BradleyLocalThresholding");

                Blob[] plateBlobs;
                if (countSigns(plate, ref processingImage, out plateBlobs)) return;

                cropAndSelectSigns(plateBlobs, plate, processingImage);

                if (show_off) FilteringTool.showEfects(new Bitmap(processingImage), "Wynik");
            }
        }

        public void ProstowanieTablic()
        {
            // dla kazdej zlokalizowanej tablicy :
            var obrocone = new List<Bitmap>();

            foreach (var plate in probably_plates)
            {
                var processingImage = FilteringTool.getBlackTopHat(plate);
                var filters = new FiltersSequence(
                    new SISThreshold(),
                    new Erosion(new short[,] {{0, 0, 0}, {0, 1, 0}, {0, 1, 0}}));
                processingImage = filters.Apply(processingImage);

                Blob[] blobsOnPlate;
                if (countSigns(plate, ref processingImage, out blobsOnPlate)) return;

                #region Wyznaczenie_Rot
                var avg_height = 0;
                var licznik = 0;
                foreach (Blob blob in blobsOnPlate)
                {
                    if (blob.Rectangle.Height > plate.Height / 3 && blob.Rectangle.Width < plate.Width / 4)
                    {
                        avg_height += blob.Rectangle.Height;
                        licznik++;
                    }
                }
                avg_height = avg_height / licznik;
                
                blobsOnPlate = blobsOnPlate.OrderBy(item => item.Rectangle.X).ToArray();
                var obszary = new List<Blob>();

                foreach (var blob in blobsOnPlate)
                {
                    if (blob.Rectangle.Height > plate.Height / 3 && blob.Rectangle.Width < plate.Width / 4)
                    {
                        if (blob.Rectangle.Height / avg_height > 0.85 && blob.Rectangle.Height / avg_height < 1.15)
                        {
                            obszary.Add(new Blob(blob)); 
                        }
                    }
                }

                if (obszary.Count > 0)
                {
                    double del_X = (-1*obszary[0].Rectangle.X + obszary[obszary.Count - 1].Rectangle.X);
                    double del_Y = (-1*obszary[0].Rectangle.Y + obszary[obszary.Count - 1].Rectangle.Y);
                    var radian = Math.Atan2(del_Y, del_X);
                    var angle = radian * (180 / Math.PI);
                    var rotacja = new RotateBicubic(angle) {KeepSize = false};
                    try
                    {
                        obrocone.Add(rotacja.Apply(plate));
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                #endregion
            }

            probably_plates = new List<Bitmap>(obrocone);

            if (show_off)
            {
                foreach (Bitmap test in probably_plates)
                {
                    FilteringTool.showEfects(test, "test");
                }
            }
        }

        public void segmentacja_Rzut_jasnosci()
        {
            foreach (Bitmap plateCandidate in probably_plates)
            {
                var temp = new Bitmap(plateCandidate);
                temp = Grayscale.CommonAlgorithms.BT709.Apply(temp);

                temp = new FiltersSequence(
                    new SISThreshold(),
                    new Invert(),
                    new BlobsFiltering
                    {
                        MinHeight = plateCandidate.Size.Height/3,
                        MinWidth = 2,
                        MaxWidth = plateCandidate.Size.Width/5
                    },
                    new Invert()).Apply(temp);

                if (show_off) FilteringTool.showEfects(new Bitmap(temp), "Filtracja szumu");

                #region RzutJasnosciIPrzerwy
                var vert = new HorizontalIntensityStatistics(temp);
                Histogram hist = vert.Gray;
                int[] wartosci = hist.Values;
                int max = hist.Values.Max();

                for (int i = 0; i < wartosci.Length; ++i)
                {
                    if (wartosci[i] < 90 * max / 100)
                    {
                        wartosci[i] = 0;
                    }
                    else
                    {
                        if (wartosci[i] > 98 * max / 100)
                        {
                            wartosci[i] = max;
                        }
                    }
                }
                wartosci[0] = wartosci[0] ;

                var punkty_x_start = new List<int>();
                var punkty_x_koniec = new List<int>();
                var licznik = 0;
                for (int x = 0; x < wartosci.Length-1; ++x)
                {
                    if (wartosci[x] > 0 && wartosci[x + 1] == 0 )
                    {
                        licznik++;
                        punkty_x_start.Add(x);
                    }
                    if (wartosci[x] == 0 && wartosci[x + 1] > 0 && punkty_x_start.Count > 0)
                    {
                        licznik++;
                        punkty_x_koniec.Add(x + 1);
                    }
                }
                #endregion

                #region Wycinanie_i_zaznaczanie_liter

                var lista_liter = new List<Bitmap>();
                var temp2 = new Bitmap(temp);
                temp = new Bitmap(plateCandidate);
                Graphics g = Graphics.FromImage(temp);
                for(int i=0; i< Math.Min(punkty_x_start.Count,punkty_x_koniec.Count); ++i)
                {
                    var figura = new Rectangle(punkty_x_start[i], 0, punkty_x_koniec[i] - punkty_x_start[i], plateCandidate.Size.Height);
                    if (figura.Width < 0)
                    {
                        continue;
                    }
                    var wycinanie = new Crop(figura);
                    
                    #region UcinanieGoryDolu
                    // uciecie gory i dolu :
                    Bitmap ucinacz = wycinanie.Apply(temp2);
                    ucinacz = Grayscale.CommonAlgorithms.BT709.Apply(ucinacz);
                    var sisTH = new SISThreshold();
                    sisTH.ApplyInPlace(ucinacz);

                    var pion = new VerticalIntensityStatistics(ucinacz);
                    Histogram pion_histogram = pion.Gray;
                    int[] wartosci_pion = pion_histogram.Values;
                    

                    int dol = plateCandidate.Height, gora = 0;
                    if (wartosci_pion[wartosci_pion.Length / 2] != 0)
                    {
                        for (int x = wartosci_pion.Length / 2; x < wartosci_pion.Length; ++x)
                        {
                            if (wartosci_pion[x] >= 255* (95*ucinacz.Width/100))
                            {
                                dol = x;
                                break;
                            }
                        }
                        for (int x = wartosci_pion.Length / 2; x >= 0; --x)
                        {
                            if (wartosci_pion[x] >= 255 * (95*ucinacz.Width/100))
                            {
                                gora = x;
                                break;
                            }
                        }
                    }
                    figura = new Rectangle(punkty_x_start[i], gora, punkty_x_koniec[i] - punkty_x_start[i], dol - gora);
                    wycinanie = new Crop(figura);
                    if (figura.Height > 0 && figura.Width > 0)
                    {
                        lista_liter.Add(wycinanie.Apply(plateCandidate));
                    }
                    #endregion

                    int left = punkty_x_start[i];
                    int top = gora;
                    int width = punkty_x_koniec[i] - punkty_x_start[i];
                    int height = dol-gora;
                    g.DrawRectangle(new Pen(Color.Red), left, top, width, height);
                }
                plate_letters.Add(new List<Bitmap>(lista_liter));
                #endregion
                if (show_off) FilteringTool.showEfects(temp, "test");
            }
        }
        #endregion

        #region Getters
        public Bitmap getImage()
        {
            return Obrazek;
        }

        public Bitmap getOriginal()
        {
            return Oryginal;
        }

        public List<List<Bitmap>> getLetters()
        {
            return plate_letters;
        }
        #endregion

        #region Reset
        public void reset()
        {
            if (Obrazek != null)
            {
                Obrazek = Grayscale.CommonAlgorithms.BT709.Apply(Oryginal);
                probably_plates = new List<Bitmap>();
                plate_letters = new List<List<Bitmap>>();
            }
        }

        public void resetLetters()
        {
            plate_letters = new List<List<Bitmap>>();
        }
        #endregion
    }
}
