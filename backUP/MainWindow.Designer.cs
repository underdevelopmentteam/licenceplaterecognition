﻿namespace ProjektMagisterski
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoadImage = new System.Windows.Forms.Button();
            this.sourcePictureBox = new System.Windows.Forms.PictureBox();
            this.groupBoxAlgorithms = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnRecognize = new System.Windows.Forms.Button();
            this.btnExtract = new System.Windows.Forms.Button();
            this.btnLocalize = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxShowSteps = new System.Windows.Forms.CheckBox();
            this.groupBoxMeasurements = new System.Windows.Forms.GroupBox();
            this.listBoxResults = new System.Windows.Forms.ListBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.sourcePictureBox)).BeginInit();
            this.groupBoxAlgorithms.SuspendLayout();
            this.groupBoxMeasurements.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadImage
            // 
            this.btnLoadImage.Location = new System.Drawing.Point(9, 19);
            this.btnLoadImage.Name = "btnLoadImage";
            this.btnLoadImage.Size = new System.Drawing.Size(134, 20);
            this.btnLoadImage.TabIndex = 0;
            this.btnLoadImage.Text = "LOAD";
            this.btnLoadImage.UseVisualStyleBackColor = true;
            this.btnLoadImage.Click += new System.EventHandler(this.btnLoadImage_Click);
            // 
            // sourcePictureBox
            // 
            this.sourcePictureBox.Location = new System.Drawing.Point(176, 125);
            this.sourcePictureBox.Name = "sourcePictureBox";
            this.sourcePictureBox.Size = new System.Drawing.Size(229, 169);
            this.sourcePictureBox.TabIndex = 1;
            this.sourcePictureBox.TabStop = false;
            // 
            // groupBoxAlgorithms
            // 
            this.groupBoxAlgorithms.Controls.Add(this.label5);
            this.groupBoxAlgorithms.Controls.Add(this.label6);
            this.groupBoxAlgorithms.Controls.Add(this.label3);
            this.groupBoxAlgorithms.Controls.Add(this.label1);
            this.groupBoxAlgorithms.Controls.Add(this.comboBox3);
            this.groupBoxAlgorithms.Controls.Add(this.comboBox2);
            this.groupBoxAlgorithms.Controls.Add(this.comboBox1);
            this.groupBoxAlgorithms.Controls.Add(this.btnRecognize);
            this.groupBoxAlgorithms.Controls.Add(this.btnExtract);
            this.groupBoxAlgorithms.Controls.Add(this.btnLocalize);
            this.groupBoxAlgorithms.Enabled = false;
            this.groupBoxAlgorithms.Location = new System.Drawing.Point(167, 12);
            this.groupBoxAlgorithms.Name = "groupBoxAlgorithms";
            this.groupBoxAlgorithms.Size = new System.Drawing.Size(244, 107);
            this.groupBoxAlgorithms.TabIndex = 2;
            this.groupBoxAlgorithms.TabStop = false;
            this.groupBoxAlgorithms.Text = "Algorytmy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Lokalizacja";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Ekstrakcja";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Rozpoznanie";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Lokalizacja";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(97, 77);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(100, 21);
            this.comboBox3.TabIndex = 5;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(97, 49);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(100, 21);
            this.comboBox2.TabIndex = 4;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(97, 21);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // btnRecognize
            // 
            this.btnRecognize.Location = new System.Drawing.Point(203, 77);
            this.btnRecognize.Name = "btnRecognize";
            this.btnRecognize.Size = new System.Drawing.Size(35, 23);
            this.btnRecognize.TabIndex = 2;
            this.btnRecognize.Text = "GO";
            this.btnRecognize.UseVisualStyleBackColor = true;
            this.btnRecognize.Click += new System.EventHandler(this.btnRecognize_Click);
            // 
            // btnExtract
            // 
            this.btnExtract.Location = new System.Drawing.Point(203, 48);
            this.btnExtract.Name = "btnExtract";
            this.btnExtract.Size = new System.Drawing.Size(35, 23);
            this.btnExtract.TabIndex = 1;
            this.btnExtract.Text = "GO";
            this.btnExtract.UseVisualStyleBackColor = true;
            this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
            // 
            // btnLocalize
            // 
            this.btnLocalize.Location = new System.Drawing.Point(203, 19);
            this.btnLocalize.Name = "btnLocalize";
            this.btnLocalize.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnLocalize.Size = new System.Drawing.Size(35, 23);
            this.btnLocalize.TabIndex = 0;
            this.btnLocalize.Text = "GO";
            this.btnLocalize.UseVisualStyleBackColor = true;
            this.btnLocalize.Click += new System.EventHandler(this.btnLocalize_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Ekstrakcja";
            // 
            // checkBoxShowSteps
            // 
            this.checkBoxShowSteps.AutoSize = true;
            this.checkBoxShowSteps.Location = new System.Drawing.Point(36, 74);
            this.checkBoxShowSteps.Name = "checkBoxShowSteps";
            this.checkBoxShowSteps.Size = new System.Drawing.Size(79, 17);
            this.checkBoxShowSteps.TabIndex = 3;
            this.checkBoxShowSteps.Text = "show steps";
            this.checkBoxShowSteps.UseVisualStyleBackColor = true;
            this.checkBoxShowSteps.CheckedChanged += new System.EventHandler(this.checkBoxShowSteps_CheckedChanged);
            // 
            // groupBoxMeasurements
            // 
            this.groupBoxMeasurements.Controls.Add(this.textBox4);
            this.groupBoxMeasurements.Controls.Add(this.listBoxResults);
            this.groupBoxMeasurements.Controls.Add(this.textBox3);
            this.groupBoxMeasurements.Controls.Add(this.textBox2);
            this.groupBoxMeasurements.Controls.Add(this.textBox1);
            this.groupBoxMeasurements.Controls.Add(this.label4);
            this.groupBoxMeasurements.Controls.Add(this.label2);
            this.groupBoxMeasurements.Controls.Add(this.label7);
            this.groupBoxMeasurements.Location = new System.Drawing.Point(417, 12);
            this.groupBoxMeasurements.Name = "groupBoxMeasurements";
            this.groupBoxMeasurements.Size = new System.Drawing.Size(233, 204);
            this.groupBoxMeasurements.TabIndex = 4;
            this.groupBoxMeasurements.TabStop = false;
            this.groupBoxMeasurements.Text = "Pomiary";
            // 
            // listBoxResults
            // 
            this.listBoxResults.FormattingEnabled = true;
            this.listBoxResults.Location = new System.Drawing.Point(12, 89);
            this.listBoxResults.Name = "listBoxResults";
            this.listBoxResults.Size = new System.Drawing.Size(215, 108);
            this.listBoxResults.TabIndex = 10;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(86, 63);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(141, 20);
            this.textBox3.TabIndex = 9;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(86, 37);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(68, 20);
            this.textBox2.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(86, 11);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(141, 20);
            this.textBox1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Lokalizacja";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Rozpoznanie";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(9, 45);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(134, 23);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.label8);
            this.groupBoxSettings.Controls.Add(this.pictureBox1);
            this.groupBoxSettings.Controls.Add(this.btnLoadImage);
            this.groupBoxSettings.Controls.Add(this.btnReset);
            this.groupBoxSettings.Controls.Add(this.checkBoxShowSteps);
            this.groupBoxSettings.Location = new System.Drawing.Point(12, 12);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(149, 282);
            this.groupBoxSettings.TabIndex = 6;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Init/Reset";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 172);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "ORGINAL";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(9, 188);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 88);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(160, 37);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(66, 20);
            this.textBox4.TabIndex = 11;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 307);
            this.Controls.Add(this.groupBoxSettings);
            this.Controls.Add(this.groupBoxMeasurements);
            this.Controls.Add(this.groupBoxAlgorithms);
            this.Controls.Add(this.sourcePictureBox);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            ((System.ComponentModel.ISupportInitialize)(this.sourcePictureBox)).EndInit();
            this.groupBoxAlgorithms.ResumeLayout(false);
            this.groupBoxAlgorithms.PerformLayout();
            this.groupBoxMeasurements.ResumeLayout(false);
            this.groupBoxMeasurements.PerformLayout();
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLoadImage;
        private System.Windows.Forms.PictureBox sourcePictureBox;
        private System.Windows.Forms.GroupBox groupBoxAlgorithms;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnRecognize;
        private System.Windows.Forms.Button btnExtract;
        private System.Windows.Forms.Button btnLocalize;
        private System.Windows.Forms.CheckBox checkBoxShowSteps;
        private System.Windows.Forms.GroupBox groupBoxMeasurements;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ListBox listBoxResults;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox4;
    }
}