using System;
using System.Collections.Generic;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;

namespace ProjektMagisterski
{
    public class RzutyJasnosci
    {
        private readonly List<List<int>> wzorceRzutowJasnosci = new List<List<int>>();

        public RzutyJasnosci(string samplesDirectory = "WzorceJasnosci\\")
        {
            WczytajRzutyJasnosci(samplesDirectory);
        }

        #region Private
        private void WczytajRzutyJasnosci(string directory = "WzorceJasnosci\\")
        {
            for (var i = '0'; i <= '9'; ++i)
            {
                var pathToFile = directory + "R" + i + ".txt";
                wzorceRzutowJasnosci.Add(CommonTools.WczytajDaneZPliku(pathToFile));
            }

            for (var i = 'A'; i <= 'Z'; ++i)
            {
                var pathToFile = directory + "R" + i + ".txt";
                wzorceRzutowJasnosci.Add(CommonTools.WczytajDaneZPliku(pathToFile));
            }

        }

        private static int[] rzut_jasnosci_obrazu(Bitmap litera)
        {
            var grayImage = Grayscale.CommonAlgorithms.BT709.Apply(litera);

            var Vstats = new VerticalIntensityStatistics(grayImage);
            var Hstats = new HorizontalIntensityStatistics(grayImage);

            var Vhistogram = Vstats.Gray;
            var Hhistogram = Hstats.Gray;
            var verticalInt = Vhistogram.Values;
            var horizontalInt = Hhistogram.Values;

            var result = new int[verticalInt.Length + horizontalInt.Length];

            for (var i = 0; i < verticalInt.Length; ++i)
            {
                result[i] = verticalInt[i];
            }

            var j = 0;
            for (var i = verticalInt.Length; i < verticalInt.Length + horizontalInt.Length; ++i)
            {
                result[i] = horizontalInt[j++];
            }

            return result;
        }

        private int maxValue(int[] wzorzecLitery, int lastIndex)
        {
            var maxV = wzorzecLitery[lastIndex];

            for (int i = lastIndex - 4; i <= lastIndex; i++)
            {
                if (wzorzecLitery[lastIndex] > maxV)
                {
                    maxV = wzorzecLitery[lastIndex];
                }
            }
            return maxV;
        }

        private int minValue(int[] wzorzecLitery, int lastIndex)
        {
            var minV = wzorzecLitery[lastIndex];

            for (int i = lastIndex - 4; i <= lastIndex; i++)
            {
                if (wzorzecLitery[lastIndex] < minV)
                {
                    minV = wzorzecLitery[lastIndex];
                }
            }
            return minV;
        }

        private char zaklasyfikuj_litere_rzuty(int[] rzutJasnosci)
        {
            var wynik = new double[36];

            for (var numer = 0; numer < 36; ++numer)
            {
                wynik[numer] = 0;
                var wzorzecLitery = wzorceRzutowJasnosci[numer];

                var wspolczynnikPodobienstwa = new List<double>();

                for (var i = 4; i < wzorzecLitery.Count; ++i)
                {
                    var WzorzecminV = minValue(wzorzecLitery.ToArray(), i) + 0.01;
                    var WzorzecmaxV = maxValue(wzorzecLitery.ToArray(), i) + 0.01;

                    var RzutMinV = minValue(rzutJasnosci, i) + 0.01;
                    var RzutMaxV = maxValue(rzutJasnosci, i) + 0.01;

                    var liczbaMin = Math.Min(WzorzecminV / RzutMinV, RzutMinV / WzorzecminV);
                    var liczbaMax = Math.Min(WzorzecmaxV / RzutMaxV, RzutMaxV / WzorzecmaxV);

                    wynik[numer] += (liczbaMin + liczbaMax) / 2;
                }
            }

            double max = wynik[0];
            int maxIndex = 0;

            for (int i = 0; i < 36; ++i)
            {
                if (wynik[i] > max)
                {
                    max = wynik[i];
                    maxIndex = i;
                }
            }

            // tutaj po "index" ustalamy wskazana litere.
            char[] tabofresult = new char[36] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            return tabofresult[maxIndex];
        }
        #endregion

        public List<string> rozpoznanie_liter_rzuty(List<List<Bitmap>> listy_liter)
        {
            var tekst = new List<string>();

            foreach (var lista in listy_liter)
            {
                string napis = "";
                foreach (var bmp in lista)
                {
                    char temp = zaklasyfikuj_litere_rzuty(rzut_jasnosci_obrazu(FilteringTool.convertToLetterStandard(bmp)));
                    napis += temp;
                }
                tekst.Add(napis);
            }
            return tekst;
        }
    }
}