﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BadanieVideo
{
    public partial class SourcePlayer : Form
    {
        public SourcePlayer()
        {
            InitializeComponent();
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public void setImage(Bitmap bmp)
        {
            pictureBox1.Image = new Bitmap(bmp);
        }
    }
}
