﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using AForge.Controls;
using AForge.Imaging.Filters;
using AForge.Robotics.TeRK;
using AForge.Video;
using AForge.Video.DirectShow;
using ProjektMagisterski;
using ProjektMagisterski.Classification;
using ProjektMagisterski.Localization;
using ProjektMagisterski.Segmentation;

namespace BadanieVideo
{
    public partial class Form1 : Form
    {
        SourcePlayer player = new SourcePlayer();

        readonly ALPRecognition recognition = new ALPRecognition();
        private VideoSourcePlayer videoSourcePlayer;
        private FileVideoSource film;
        private Bitmap image;
        private object locker = new object();
        private Stopwatch zegarek = new Stopwatch();

        public Form1()
        {
            InitializeComponent();


            sourcePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            sourcePictureBox.Update();
            sourcePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            sourcePictureBox.Update();


            comboBox1.Items.AddRange(LocalizationAlgorithm.getAlgorithms());
            comboBox1.SelectedIndex = 0;

            comboBox2.Items.AddRange(SegmentationAlgorithm.getAlgorithms());
            comboBox2.SelectedIndex = 0;

            comboBox3.Items.AddRange(RozpoznawanieLiter.getAlgorithms());
            comboBox3.SelectedIndex = 0;

            groupBoxAlgorithms.Enabled = true;
        }


        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            var okienko = new OpenFileDialog();
            videoSourcePlayer = new VideoSourcePlayer();

            recognition.selectLocalizationAlgorithm((string) comboBox1.SelectedItem);
            recognition.selectSegmentationAlgorithm((string) comboBox2.SelectedItem);

            if (okienko.ShowDialog() == DialogResult.OK)
            {
                film = new FileVideoSource(okienko.FileName);
                film.NewFrame += film_NewFrame;
                videoSourcePlayer.VideoSource = film;
                videoSourcePlayer.Start();
                player.Show(); // TODO : remove
            }
        }

        void film_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                lock (locker)
                {
                    //player.setImage(eventArgs.Frame);

                    var width = eventArgs.Frame.Width;
                    var height = eventArgs.Frame.Height;
                    var cropper =
                        new Crop(new Rectangle((int) (0.0*width), (int) (0.5*height), (int) (1*width),
                            (int) (0.5*height)));
                    var cropped = cropper.Apply(eventArgs.Frame);
                    long lastTime = 0;

                    zegarek.Restart();

                    lastTime = zegarek.ElapsedMilliseconds;
                    recognition.localizePlate(cropped);
                    textBox1.Invoke(new Action(() =>
                        textBox1.Text = (zegarek.ElapsedMilliseconds - lastTime).ToString())); // raport time

                    var candidatePlates = recognition.getPlateCandidates();

                    lastTime = zegarek.ElapsedMilliseconds;
                    recognition.segmentatePlates();
                    textBox2.Invoke(new Action(() =>
                        textBox2.Text = (zegarek.ElapsedMilliseconds - lastTime).ToString())); // raport time

                    var index = 0;
                    comboBox3.Invoke(new Action(() => index = comboBox3.SelectedIndex));

                    lastTime = zegarek.ElapsedMilliseconds;
                    var result = recognition.classifyLetters(index);
                    textBox3.Invoke(new Action(() =>
                        textBox3.Text = (zegarek.ElapsedMilliseconds - lastTime).ToString())); // raport time

                    recognition.resetSegmentation();

                    zegarek.Stop();

                    var plates = new List<LicencePlateResult>();

                    textBox4.Invoke(new Action(() =>
                        textBox4.Text = ((double) 1000/zegarek.ElapsedMilliseconds).ToString() + " s")); // raport time

                    for (int i = 0; i < result.Count; i++)
                    {
                        if (result[i].Length > 4)
                        {
                            plates.Add(new LicencePlateResult(result[i], candidatePlates[i]));
                        }
                    }

                    if (result.Count != 0)
                    {
                        listBoxResults.Invoke(new Action(() => listBoxResults.Items.AddRange(plates.ToArray())));
                        listBoxResults.Invoke(
                            new Action(() => listBoxResults.SelectedIndex = listBoxResults.Items.Count - 1));
                    }

                    sourcePictureBox.Image = recognition.getLocalizedPlate();
                }
            }
            catch (Exception e){ /* handle break of video reading */}
        }

        private void listBoxResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            var temp = listBoxResults.Items[listBoxResults.SelectedIndex] as LicencePlateResult;
            player.setImage(temp.getImage());
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e){}
    }

    internal class LicencePlateResult
    {
        private string licencePlate;
        private Bitmap image;

        public LicencePlateResult(string p_licence, Bitmap bmp)
        {
            licencePlate = p_licence;
            image = new Bitmap(bmp);
        }

        public override string ToString()
        {
            return licencePlate;
        }

        public Bitmap getImage()
        {
            return image;
        }
    }
}
