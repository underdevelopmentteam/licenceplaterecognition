﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ProjektMagisterski.Classification;

namespace SOMLearning
{
    public partial class Form1 : Form
    {
        private ClassificationUsingSOM classifier;
        private string dir = "Siec\\";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClassificationUsingSOM.createNetwork(dir + "ssnSOMxX34", 12 * 18, 36);

            classifier = new ClassificationUsingSOM(dir + "ssnSOMxX34");

            var watek = new Thread(() => classifier.uczenieSOM(1000000));
            watek.Start();

            var updateThread = new Thread(() =>
            {

                while (true)
                {
                    if (classifier != null)
                    textBox1.Invoke(new Action(() => textBox1.Text = classifier.learningError.ToString()));
                    Thread.Sleep(50);
                }
                
            });
            updateThread.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            classifier = new ClassificationUsingSOM(dir + "ssnSOM");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var Okno_wczytywania_pliku = new OpenFileDialog();
            if (Okno_wczytywania_pliku.ShowDialog() == DialogResult.OK)
            {
                var fileName = Okno_wczytywania_pliku.FileName;
                var image = new Bitmap(fileName);
                var list = new List<List<Bitmap>> {new List<Bitmap> {image}};
                var result = classifier.rozpoznaj(list, true);
                MessageBox.Show(result.First());
            }
        }
    }
}
